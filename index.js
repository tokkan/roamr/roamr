import 'react-native-gesture-handler'
import { registerRootComponent } from 'expo'
import Config from 'react-native-config'
import Axios from 'axios'
import { AppContainer } from './src/screens/app/app-container'

Axios.defaults.baseURL = Config.GATEWAY_API_URL

// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in the Expo client or in a native build,
// the environment is set up appropriately
registerRootComponent(AppContainer)
