import { ProviderTypes } from "../../store/auth";

export interface IAuthHeaders {
    id: string
    access_token: string
    social_provider: ProviderTypes
}