// export enum PinTypes {
//     'Camping',
//     'Cafe',
//     'Restaurant',
//     'Trail',
//     'Swimming',
//     'Fishing',
//     'BNB',
//     'Rest area',
//     'Store',
//     'Heritage',
//     'Nature reserve',
//     'POI',
//     'Event',
// }

// export enum TrailTypes {
//     'Hiking',
//     'Biking',
// }

// export enum StoreTypes {
//     'Physical items',
//     'Consumables',
//     'Medicine',
// }

// export type AllPinTypes =
//     PinTypes |
//     TrailTypes |
//     StoreTypes

export type PinTypes =
    'camping' |
    'cafe' |
    'restaurant' |
    'trail' |
    'swimming' |
    'fishing' |
    'bnb' |
    'rest_area' |
    'store' |
    'heritage' |
    'nature_reserve' |
    'poi' |
    'event' |
    'hiking' |
    'biking'