export default {
    login: {
        facebook_login: "Logga in med Facebook",
    },
    settings: {
        settings_title: 'Inställningar',
        theme_title: 'Tema inställningar',
        system_theme: 'Använd systeminställningar',
        map_theme: 'Karttema',
        language: 'Språk',
    },
}