import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { FollowSheetScreen } from './follow-action-sheet'

export type RootStackFollowActionSheetRoutesParamList = {
    ['Follow sheet']: {
        user_id: string
        following: boolean
        post_notifications: 'following' | 'post_notifications' | 'muted'
        // other props, like following, post notifications and such, so we dont have to hit the API twice?
    }
}

const Stack = createStackNavigator<RootStackFollowActionSheetRoutesParamList>()

export const FollowSheetNavigator = () => {
    return (
        <Stack.Navigator
            initialRouteName={'Follow sheet'}
            headerMode='none'
        >
            <Stack.Screen
                name={'Follow sheet'}
                component={FollowSheetScreen}
            />
        </Stack.Navigator>
    )
}