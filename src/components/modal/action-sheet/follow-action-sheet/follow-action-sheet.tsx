import React, { useState } from 'react'
import { Layout, Text, Divider, TopNavigation, TabView, Tab, Button, TopNavigationAction, Toggle } from '@ui-kitten/components'
import { StyleSheet, View } from 'react-native'
import { BackIcon, RightArrowIcon } from './../../../../models'
import { IFollowMuteSettings, IFollowNotificationSettings } from './../../../../views/user/prospect'

interface ISwitchControlProps {
    text: string
    value: boolean
    handleValue: ((value: boolean) => void | undefined)
}

interface IProps {
    username: string
    unfollow: any
    notificationSettings: IFollowNotificationSettings
    muteSettings: IFollowMuteSettings
    handleNotificationChange: <T extends IFollowNotificationSettings, K extends keyof IFollowNotificationSettings>(prop: K) => (value: T[K]) => void
    handleMuteChange: <T extends IFollowMuteSettings, K extends keyof IFollowMuteSettings>(prop: K) => (value: T[K]) => void
}

export const FollowSheetScreen: React.FC<IProps> = ({
    username,
    unfollow,
    notificationSettings,
    muteSettings,
    handleNotificationChange,
    handleMuteChange,
}) => {
    const [selectedIndex, setSelectedIndex] = useState(0)

    const goToView = (index: number) => {
        setSelectedIndex(index)
    }

    const goBack = () => {
        setSelectedIndex(0)
    }

    const BackAction = () => (
        <TopNavigationAction
            icon={BackIcon}
            onPress={goBack}
        />
    )

    const SwitchControl: React.FC<ISwitchControlProps> = ({
        text,
        value,
        handleValue,
    }) => (
            <View
                style={styles.switchControl}
            >
                <Text>{text}</Text>
                <Toggle
                    style={{ marginLeft: 20, marginRight: 10 }}
                    checked={value}
                    onChange={handleValue}
                />
            </View>
        )

    const getHeaderText = () => {
        switch (selectedIndex) {
            case 0:
                return username
            case 1:
                return 'Notifications'
            case 2:
                return 'Mute'
            default:
                return username
        }
    }

    return (
        <Layout
            style={{
                paddingBottom: 10,
                paddingTop: 0,
                paddingHorizontal: 0,
            }}
        >
            <TopNavigation
                alignment='center'
                title={getHeaderText()}
                accessoryLeft={selectedIndex > 0 ? BackAction : undefined}
            />
            <Divider />
            <TabView
                selectedIndex={selectedIndex}
                indicatorStyle={{
                    backgroundColor: 'transparent',
                }}
            >
                <Tab>
                    <Layout style={styles.mainTabContainer}>
                        <Button
                            style={styles.button}
                            status='basic'
                            appearance='ghost'
                            onPress={() => goToView(1)}
                            accessoryRight={RightArrowIcon}
                        >
                            Post notifications
                        </Button>
                        {/* <Button
                            style={styles.button}
                            status='basic'
                            appearance='ghost'
                            onPress={() => goToView(2)}
                            accessoryRight={RightArrowIcon}
                        >
                            Mute
                        </Button> */}
                        <Button
                            style={styles.button}
                            status='basic'
                            appearance='ghost'
                            onPress={unfollow}
                        >
                            Unfollow
                        </Button>
                    </Layout>
                </Tab>
                <Tab>
                    <Layout style={styles.tabContainer}>
                        <SwitchControl
                            text={'Posts'}
                            value={notificationSettings.notifyPosts}
                            handleValue={handleNotificationChange('notifyPosts')}
                        />
                        <SwitchControl
                            text={'Events'}
                            value={notificationSettings.notifyEvents}
                            handleValue={handleNotificationChange('notifyEvents')}
                        />
                        {/* <SwitchControl
                            text={'Stories'}
                            value={notificationSettings.notifyStories}
                            handleValue={handleNotificationChange('notifyStories')}
                        /> */}
                    </Layout>
                </Tab>
                <Tab>
                    <Layout style={styles.tabContainer}>
                        <SwitchControl
                            text={'Posts'}
                            value={muteSettings.mutePosts}
                            handleValue={handleMuteChange('mutePosts')}
                        />
                        <SwitchControl
                            text={'Events'}
                            value={muteSettings.muteEvents}
                            handleValue={handleMuteChange('muteEvents')}
                        />
                        {/* <SwitchControl
                            text={'Stories'}
                            value={muteSettings.muteStories}
                            handleValue={handleMuteChange('muteStories')}
                        /> */}
                    </Layout>
                </Tab>
            </TabView>
        </Layout>
    )
}

const styles = StyleSheet.create({
    mainTabContainer: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    tabContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 15,
    },
    button: {
        width: '100%',
        marginHorizontal: 0,
        justifyContent: 'space-between'
    },
    switchControl: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 10,
    }
})