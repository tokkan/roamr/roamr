import React from 'react'
import { ReactNativeModal } from 'react-native-modal'
import { Layout, useTheme } from '@ui-kitten/components'

interface IProps {
    isOpen: boolean
    setIsOpen: React.Dispatch<React.SetStateAction<boolean>>
    children: any
}

export const BottomActionSheet: React.FC<IProps> = ({
    isOpen,
    setIsOpen,
    children,
}) => {
    const theme = useTheme()

    return (
        <ReactNativeModal
            // propagateSwipe
            isVisible={isOpen}
            avoidKeyboard={true}
            coverScreen={false}
            // animationIn='slideInUp'
            // animationInTiming={300}
            // animationOut='slideOutDown'
            // animationOutTiming={300}
            // swipeThreshold={100}
            // swipeDirection={['up', 'left', 'right', 'down']}
            swipeDirection={['down']}
            onSwipeComplete={params => {
                if (params.swipingDirection === 'down') {
                    setIsOpen(false)
                }
            }}
            onBackdropPress={() => setIsOpen(false)}
            style={{
                justifyContent: 'flex-end',
                margin: 0,
            }}
        >
            <Layout
                style={{
                    borderTopLeftRadius: 15,
                    borderTopRightRadius: 15,
                    // height: 20,
                    paddingTop: 10,
                    width: '100%',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
            >
                <Layout
                    style={{
                        height: 7,
                        width: 30,
                        backgroundColor: theme['text-hint-color'],
                        borderTopStartRadius: 10,
                        borderTopEndRadius: 10,
                        borderBottomStartRadius: 10,
                        borderBottomEndRadius: 10,
                    }}
                />
            </Layout>
            <Layout
                style={{
                    paddingBottom: 15,
                }}
            >
                {children}
            </Layout>
        </ReactNativeModal>
    )
}