import {
    TopNavigation, Card, List, ListItem, TopNavigationAction, Modal
} from '@ui-kitten/components'
import { RenderProp } from '@ui-kitten/components/devsupport'
import React, { Dispatch, SetStateAction } from 'react'
import { ImageProps } from 'react-native'
import { CloseIcon } from './../../../../models'

export interface IData {
    title: string
    onPress: Function
    // icon?: (props?: Partial<ImageProps> | undefined) => React.ReactElement<ImageProps>
    icon?: RenderProp<Partial<ImageProps>>
}

interface IProps {
    menuVisible: boolean
    setMenuVisible: Dispatch<SetStateAction<boolean>>
    data: IData[]
}

export const OptionsListModal: React.FC<IProps> = ({
    menuVisible,
    setMenuVisible,
    data,
}) => {
    const CloseAction = () => (
        <TopNavigationAction
            icon={CloseIcon}
            onPress={() => setMenuVisible(false)}
        />
    )

    const renderOptionsItem = ({
        item,
        index,
    }: {
        item: IData,
        index: number,
    }) => {
        return (
            <ListItem
                key={index}
                title={item.title}
                accessoryLeft={item.icon}
                onPress={() => {
                    setMenuVisible(false)
                    item.onPress()
                }}
            />
        )
    }

    return (
        <Modal
            visible={menuVisible}
            style={{
                width: '100%',
            }}
            backdropStyle={{
                backgroundColor: 'rgba(0, 0, 0, 0.5)',
            }}
            onBackdropPress={() => setMenuVisible(false)}
        >
            <TopNavigation
                accessoryRight={CloseAction}
            />
            <Card disabled={true}>
                <List
                    data={data}
                    renderItem={renderOptionsItem}
                />
            </Card>
        </Modal>
    )
}