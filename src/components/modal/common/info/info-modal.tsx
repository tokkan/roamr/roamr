import React from 'react'
import { StyleSheet, View } from 'react-native'
import { Button, Card, Modal, Text, ListItem, List, Divider, Layout } from '@ui-kitten/components'
import { i18n } from './../../../../locales/localization'

interface IHeaderProps {
    props: any
    title: string
}

const Header: React.FC<IHeaderProps> = ({ props, title }) => {
    return (
        <View {...props}>
            <Text category='h6'>
                {title}
            </Text>
        </View>
    )
}

interface IFooterProps {
    props: any
    setVisible: React.Dispatch<React.SetStateAction<boolean>>
}

const Footer: React.FC<IFooterProps> = ({
    props,
    setVisible,
}) => (
        <View {...props} style={[props.style, styles.footerContainer]}>
            <Button
                appearance='outline'
                onPress={() => setVisible(false)}
            >
                {i18n.t('common.close')}
            </Button>
        </View>
    )

interface IProps {
    title: string
    description: string
    list: string[]
    visible: boolean
    setVisible: React.Dispatch<React.SetStateAction<boolean>>
}

export const InformationModal: React.FC<IProps> = ({
    title,
    description,
    list,
    visible,
    setVisible,
}) => {

    const renderItem = ({ item, index }: any) => (
        <ListItem
            style={{
                marginVertical: 0,
                paddingVertical: 5,
            }}
            title={item}
        />
    )

    return (
        <Modal
            style={{
                width: '100%',
            }}
            visible={visible}
            backdropStyle={styles.backdrop}
            onBackdropPress={() => setVisible(false)}
        >
            <Card
                disabled={true}
                status='info'
                // header={props => Header({ props, title })}
                header={props =>
                    <Header props={props} title={title} />}
                footer={props =>
                    <Footer props={props} setVisible={setVisible} />}
            >
                <Layout style={{
                    marginBottom: 10,
                }}>
                    <Text>
                        {description}
                    </Text>
                </Layout>
                <List
                    ItemSeparatorComponent={Divider}
                    data={list}
                    renderItem={renderItem}
                />
            </Card>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        minHeight: 192,
    },
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    footerContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    footerControl: {
        marginHorizontal: 2,
    },
})