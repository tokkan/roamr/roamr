import React, { Dispatch, FC, SetStateAction } from 'react'
import { GestureResponderEvent, Pressable, StyleSheet } from 'react-native'

interface IProps {
    setPressing: Dispatch<SetStateAction<boolean>>
    onPress?: (event: GestureResponderEvent) => void
    onLongPress?: (event: GestureResponderEvent) => void
    onTouchStart?: (event: GestureResponderEvent) => void
    onTouchMove?: (event: GestureResponderEvent) => void
    onTouchEndCapture?: (event: GestureResponderEvent) => void
    onTouchEnd?: (event: GestureResponderEvent) => void
    onTouchCancel?: (event: GestureResponderEvent) => void
}

export const Shutter: FC<IProps> = ({
    setPressing,
    onPress,
    onLongPress,
    onTouchStart,
    onTouchMove,
    onTouchEnd,
    onTouchEndCapture,
    onTouchCancel,
}) => {
    return (
        <>
            <Pressable
                // style={[
                //     styles.shutter,
                // ]}
                style={
                    ({ pressed }) => [
                        {
                            borderColor: pressed ?
                                'red' :
                                '#FFF',
                        },
                        styles.shutter
                    ]
                }
                onPress={onPress}
                onLongPress={onLongPress}
                onPressOut={() => setPressing(false)}
                onTouchStart={onTouchStart}
                onTouchMove={onTouchMove}
                onTouchEnd={onTouchEnd}
                onTouchEndCapture={onTouchEndCapture}
                onTouchCancel={onTouchCancel}
            />
        </>
    )
}

const styles = StyleSheet.create({
    shutter: {
        height: 64,
        width: 64,
        borderRadius: 100,
        // borderColor: '#fff',
        borderWidth: 2,
        alignSelf: 'center',
        // marginBottom: 50,
    },
})