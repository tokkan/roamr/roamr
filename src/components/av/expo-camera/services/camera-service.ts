import React, { RefObject } from 'react'
import { Camera, CameraPictureOptions, CameraRecordingOptions } from 'expo-camera'
import { PermissionsAndroid } from 'react-native'
import { Audio } from 'expo-av'

export const requestCameraPermission = async () => {
    const { status } = await Camera.requestPermissionsAsync()
    return status
}

export const requestAudioPermission = async () => {
    const { status } = await Audio.requestPermissionsAsync()
    return status
}

export const snapImage = async (
    ref: RefObject<Camera>,
    options?: CameraPictureOptions,
) => {
    if (ref.current) {
        try {
            const data = await ref.current.takePictureAsync(options)
            return data
        } catch (error) {
            console.debug('error')
            console.debug(error)
            return undefined
        }
    }
}

export const recordVideo = async (
    ref: RefObject<Camera>,
    options?: CameraRecordingOptions,
) => {
    if (ref.current) {
        try {
            const data = await ref.current.recordAsync(options)
            return data
        } catch (error) {
            console.debug('record error')
            console.debug(error)
            stopRecordVideo(ref)
        }
    }
}

export const stopRecordVideo = (
    ref: RefObject<Camera>,
) => {
    if (ref.current) {
        ref.current.stopRecording()
    }
}

export const resumePreview = (ref: RefObject<Camera>) => {
    if (ref.current) {
        ref.current.resumePreview()
    }
}

export const pausePreview = (ref: RefObject<Camera>) => {
    if (ref.current) {
        ref.current.pausePreview()
    }
}

/**
 * 
 * @param ref 
 * @param mode 
 * on flashes when taking a picture
 * off won't flash
 * auto will fire flash if required
 * torch turns on flash during the preview 
 */
export const setFlashMode = (
    mode: typeof Camera.Constants.FlashMode,
) => {
}

/**
 * 
 * @param ref 
 * @param mode 
 * on will autofocus
 * off won't autofocus
 */
export const toggleAutoFocus = (
    mode: typeof Camera.Constants.FlashMode,
) => {
    return mode === Camera.Constants.FlashMode.on ?
        Camera.Constants.FlashMode.off :
        Camera.Constants.FlashMode.on
}