import React, { FC, useEffect, useRef, useState } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Layout } from '@ui-kitten/components'
import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
// import { BarCodeScanner } from 'expo'
import { Camera, CameraPictureOptions, CameraRecordingOptions, FaceDetectionResult, PermissionStatus } from 'expo-camera'
// import * as FaceDetector from 'expo-face-detector'
import { RootStackCameraRoutesParamList } from '../../../screens/routes/camera'
import { recordVideo, requestAudioPermission, requestCameraPermission, resumePreview, snapImage, stopRecordVideo } from './services/camera-service'
import { TopNavigationComponent } from '../../top-navigation/top-navigation-stack'
import { Shutter } from './components/shutter'
import { ICameraRouteParam } from '../../../screens/routes'

type ScreenRouteProp = RouteProp<
    RootStackCameraRoutesParamList,
    'Camera'
>

type ScreenNavigationProp = StackNavigationProp<
    RootStackCameraRoutesParamList,
    'Camera'
>

interface IProps {
    route: ScreenRouteProp
    navigation: ScreenNavigationProp
}

// TODO: See if it's possible to unmount screen at unfocus, since the camera can turn black when navigated back to.
export const CameraScreen: FC<IProps> = ({
    route,
    navigation,
}) => {
    const ref = useRef<Camera>(null)
    const [hasPermission, setHasPermission] =
        useState(false)
    const [isCameraReady, setIsCameraReady] = useState(false)
    const [cameraType, setCameraType] = useState(Camera.Constants.Type.front)
    const [quality, setQuality] = useState(0)
    const [flashMode, setFlashMode] = useState(Camera.Constants.FlashMode.off)
    const [autoFocus, setAutoFocus] = useState(Camera.Constants.AutoFocus.on)
    const [zoom, setZoom] = useState(0)
    const [whiteBalance, setWhiteBalance] =
        useState(Camera.Constants.WhiteBalance.auto)
    const [focusDepth, setFocusDepth] = useState(0)
    const [ratio, setRatio] = useState('4:3')
    const [ratios, setRatios] = useState<string[]>([])
    const [pictureSizes, setPictureSizes] = useState<string[]>([])
    const [pictureSize, setPictureSize] = useState('720x480')
    const [videoStabilizationMode, setVideoStabilizationMode] =
        useState(Camera.Constants.VideoStabilization.standard)
    const [faceDetectorSettings] = useState()
    const [cameraOptions, setCameraOptions] = useState<CameraPictureOptions>({
        base64: false,
        fastMode: true,
        // imageType: ''
        quality: 0.5,
    })
    const [videoOptions, setVideoOptions] = useState<CameraRecordingOptions>({
        maxDuration: 10,
        // maxFileSize: 10000,
        quality: 0.5,
    })
    const [pressing, setPressing] = useState(false)

    useEffect(() => {
        const focus = navigation.addListener('focus', () => {
            if (ref.current) {
                ref.current.resumePreview()
            }
        })

        const unsubscribe = navigation.addListener('blur', () => {
            // source.cancel()
            if (ref.current) {
                ref.current.pausePreview()
            }
        })

        return () => {
            focus
            unsubscribe
            // source.cancel()
        }
    }, [navigation])

    useEffect(() => {
        checkCameraAccess()
    }, [])

    useEffect(() => {
        if (ref.current) {
            if (!pressing) {
                // ref.current.stopRecording()
                stopRecordVideo(ref)
            }
        }
    }, [pressing])

    const checkCameraAccess = async () => {
        let status = await requestCameraPermission()
        let audioStatus = await requestAudioPermission()
        setHasPermission(status === 'granted' && audioStatus === 'granted')

        if (status !== 'granted' || audioStatus !== 'granted') {
            navigation.goBack()
        }
    }

    const onCameraReady = async () => {
        console.debug('camera is ready')
        setIsCameraReady(true)
        if (ref.current) {
            resumePreview(ref)
            let r = await ref.current.getSupportedRatiosAsync()
            console.debug(r)
            setRatios(r)
            let p = await ref.current.getAvailablePictureSizesAsync(r[0])
            // setPictureSizes(p)
            console.debug(p)
            setPictureSize(p.reverse()[0])
        }
    }

    const toggleCameraType = () => {
        cameraType === Camera.Constants.Type.front ?
            Camera.Constants.Type.back :
            Camera.Constants.Type.front
    }

    const handleFlashMode = () => {
    }

    const toggleAutoFocus = () => {
    }

    const handleZoom = () => {
    }

    /**
     * auto,sunny,cloudy,shadow,fluorescent,incandescent
     */
    const handleWhiteBalance = () => {
    }

    // const onFacesDetected = (faces: FaceDetectionResult) => {
    // }

    // const onBarCodeScanned = (e: BarCodeScanningResult) => {
    // }

    const snap = async () => {
        // let data = await snapImage(ref, cameraOptions)
        if (ref.current) {
            try {
                // const data = await ref.current.takePictureAsync(cameraOptions)
                const data = await ref.current.takePictureAsync()

                console.log('snap')
                console.log(data)

                if (data === undefined || data === null) {
                    console.debug('failed to take image')
                    navigation.goBack()
                    return
                }

                const file: ICameraRouteParam = {
                    source: data.uri,
                    name: 'photo.jpg',
                    type: 'image/jpeg',
                }

                navigation.navigate('CameraPreview', {
                    asset: file,
                    type: 'image',
                })
            } catch (error) {
                console.log('error')
                console.log(error)
                console.error(error)
            }
        }
    }

    const record = async () => {
        try {
            setPressing(true)
            // const data = await recordVideo(ref)
            const data = await recordVideo(ref, {
                // maxDuration: 10,
                quality: 0.5,
            })
            if (data === undefined || data === null) {
                console.debug('failed to take record')
                navigation.popToTop()
                return
            }

            console.debug('record')
            console.debug(data)

            const file: ICameraRouteParam = {
                source: data.uri,
                name: 'video.mp4',
                type: 'video/mp4',
            }

            navigation.navigate('CameraPreview', {
                asset: file,
                type: 'image',
            })
        } catch (error) {
            setPressing(false)
            stopRecordVideo(ref)
            console.log('error')
            console.log(error)
            console.error(error)
        }
    }

    return (
        // <View
        //     style={{ flex: 1 }}
        // >
        <Camera
            style={{
                flex: 1,
                // height: '100%',
            }}
            ref={ref}
            useCamera2Api={true}
            ratio={ratio}
            pictureSize={pictureSize}
            // type={cameraType}
            // flashMode={flashMode}
            // autoFocus={autoFocus}
            // zoom={zoom}
            // whiteBalance={whiteBalance}
            // focusDepth={focusDepth}
            // videoStabilizationMode={videoStabilizationMode}
            // faceDetectorSettings={}
            // barCodeScannerSettings={{
            //     barCodeTypes: [BarCodeScanner.Constants.BarCodeType.qr]
            // }}
            onMountError={e => {
                console.debug(e.message)
                stopRecordVideo(ref)
            }}
            onCameraReady={onCameraReady}
        // // onFacesDetected={e => console.debug('face detected')}
        // // onBarCodeScanned={e => console.debug('on barcode readed')}
        // onTouchStart={e => console.debug('on touch start')}
        // onTouchMove={e => console.debug('on touch move')}
        // onTouchEnd={e => console.debug('on touch end')}
        // onTouchEndCapture={e => console.debug('on touch end capture')}
        // onTouchCancel={e => console.debug('on touch cancel')}
        >
            <TopNavigationComponent
                navigation={navigation}
                title=''
                styles={{
                    backgroundColor: 'transparent',
                    // borderColor: 'transparent',
                    // borderWidth: 0,
                }}
                useDivider={false}
            />
            <View
                style={styles.bottomBar}
            >
                <Shutter
                    setPressing={setPressing}
                    // onPress={async () => await snap()}
                    onPress={snap}
                    onLongPress={record}
                />
            </View>
        </Camera>
        // </View>
    )
}

const styles = StyleSheet.create({
    bottomBar: {
        // flex: 1,
        position: 'absolute',
        bottom: 0,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        paddingBottom: 50,
    },
})