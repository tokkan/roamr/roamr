import { Button } from '@ui-kitten/components'
import React, { FC } from 'react'
import { StyleSheet } from 'react-native'
import { CheckMarkOutlineIcon } from '../../../../models'

interface IProps {
    type: 'accept' | 'decline'
    onPress: () => void
}

export const FAB: FC<IProps> = ({
    type,
    onPress,
}) => {

    const renderIcon = () => {
        return (
            <CheckMarkOutlineIcon />
        )
    }

    return (
        <Button
            style={styles.button}
            accessoryLeft={renderIcon}
            onPress={onPress}
        />
    )
}

const styles = StyleSheet.create({
    button: {
        height: 64,
        width: 64,
        borderRadius: 100,
        backgroundColor: 'transparent',
        // borderColor: '#fff',
        borderWidth: 2,
        alignSelf: 'center',
        // marginBottom: 50,
    },
})