import React, { FC, useRef } from 'react'
import { Image, StyleSheet, View } from 'react-native'
import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import { Layout } from '@ui-kitten/components'
import { ICameraRouteParam, RootStackCreatePostRoutesParamList } from './../../../screens/routes'
import { ImagePreview } from './image/image-preview'
import { FAB } from './components/FAB'

type ScreenRouteProp = RouteProp<
    RootStackCreatePostRoutesParamList,
    'CameraPreview'
>

type ScreenNavigationProp = StackNavigationProp<
    RootStackCreatePostRoutesParamList,
    'CameraPreview'
>

interface IProps {
    route: ScreenRouteProp
    navigation: ScreenNavigationProp
}

// interface IProps {
//     asset: ICameraRouteParam
//     type: 'image' | 'video'
// }

export const CameraPreviewScreen: FC<IProps> = ({
    // asset,
    // type,
    navigation,
    route,
}) => {
    const {
        type,
        asset,
    } = route.params
    // const imageRef = useRef<Image>(null)
    // const videoRef = useRef(null)

    console.debug('Camera Preview')
    console.debug(route.params)

    let assets: ICameraRouteParam[] = [
        asset,
    ]

    const acceptImage = () => {
        navigation.navigate('CreatePost', assets)
    }

    const Preview = () => {
        switch (type) {
            case 'image':
                return (
                    <ImagePreview
                        // ref={imageRef}
                        source={asset.source}
                    />
                )

            case 'video':
                return (
                    // <VideoPreview
                    //     ref={videoRef}
                    //     source={asset.source}
                    // />
                    <ImagePreview
                        // ref={imageRef}
                        source={asset.source}
                    />
                )
        }
    }

    return (
        <Layout
            style={{ flex: 1 }}
        >
            <Preview />
            <View
                style={styles.bottomBar}
            >
                <FAB
                    type='accept'
                    onPress={acceptImage}
                />
            </View>
        </Layout>
    )
}

const styles = StyleSheet.create({
    bottomBar: {
        // flex: 1,
        position: 'absolute',
        bottom: 0,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        paddingBottom: 50,
    },
})