import React, { FC, RefObject } from 'react'
import { Image } from 'react-native'

interface IProps {
    // ref: RefObject<Image>
    source: string
}

export const ImagePreview: FC<IProps> = ({
    // ref,
    source,
}) => {
    return (
        <Image
            // ref={ref}
            style={{
                flex: 1,
                width: '100%',
                height: '100%',
            }}
            source={{
                // uri: `data:image/jpeg;base64,${asset.source}`,
                uri: source,
            }}
        />
    )
}