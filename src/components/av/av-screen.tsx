import React, { FC } from 'react'
import { View } from 'react-native'
import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import { RootStackCameraRoutesParamList } from '../../screens/routes/camera'
import { CameraScreen } from './expo-camera/camera-screen'

type ScreenRouteProp = RouteProp<
    RootStackCameraRoutesParamList,
    'Camera'
>

type ScreenNavigationProp = StackNavigationProp<
    RootStackCameraRoutesParamList,
    'Camera'
>

interface IProps {
    route: ScreenRouteProp
    navigation: ScreenNavigationProp
}

export const AVScreen: FC<IProps> = ({
    route,
    navigation,
}) => {

    return (
        <View
            style={{ flex: 1 }}
        >
            <CameraScreen
                navigation={navigation}
                route={route}
            />
        </View>
    )
}