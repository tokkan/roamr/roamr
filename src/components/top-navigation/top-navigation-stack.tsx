import React from 'react'
import { ImageProps, StyleProp, ViewStyle } from 'react-native'
import {
    TopNavigation, TopNavigationAction, Icon, Divider
} from '@ui-kitten/components'
import { IAccessory } from '.'
import { RenderProp } from '@ui-kitten/components/devsupport'

const BackIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='arrow-ios-back-outline' />
    )

interface IProps {
    navigation: any
    title: string
    onGoBackCallback?: () => void
    replaceNavigate?: () => void
    titleAlignment?: 'center' | 'start'
    rightAccessories?: IAccessory[]
    styles?: StyleProp<ViewStyle>
    useDivider?: boolean
}

export const TopNavigationComponent: React.FC<IProps> = ({
    navigation,
    title,
    onGoBackCallback,
    replaceNavigate,
    titleAlignment = 'start',
    rightAccessories = [],
    styles,
    useDivider = true,
}) => {
    const goBack = () => {
        if (replaceNavigate === undefined) {
            navigation.goBack()

            if (onGoBackCallback !== undefined) {
                onGoBackCallback()
            }
        } else {
            replaceNavigate()
        }
    }

    const RenderDivider = () => {
        if (useDivider === undefined || useDivider === false) {
            return (<></>)
        } else {
            return (
                <Divider />
            )
        }
    }

    const renderAction = (item: IAccessory, index: number) => {
        if (item !== undefined) {
            return (
                <TopNavigationAction
                    key={index}
                    icon={item.icon}
                    onPress={item.action}
                />
            )
        } else {
            return <></>
        }
    }

    const renderRightActions: RenderProp<{}> = () => {
        return (
            <>
                {rightAccessories.map((item, i) => (
                    renderAction(item, i)
                ))}
            </>
        )
    }

    const BackAction = () => (
        <TopNavigationAction
            icon={BackIcon}
            onPress={goBack}
        />
    )

    return (
        <>
            <TopNavigation
                style={styles}
                title={title}
                alignment={titleAlignment}
                accessoryLeft={BackAction}
                accessoryRight={renderRightActions}
            />
            <RenderDivider />
        </>
    )
}