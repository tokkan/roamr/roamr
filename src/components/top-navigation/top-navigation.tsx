import React, { useEffect } from 'react'
import { Image } from 'react-native'
import { DrawerActions } from '@react-navigation/native'
import {
    TopNavigation, TopNavigationAction, Divider} from '@ui-kitten/components'
import { RenderProp } from '@ui-kitten/components/devsupport'
import { MenuIcon } from './../../models/icons'
import { IAccessory } from '.'

interface IProps {
    navigation: any
    hasMenuAction: boolean
    hasDefaultTitle: boolean
    defaultAction?: (...props: any[]) => void
    title?: string
    rightAccessories?: IAccessory[]
    options?: IAccessory[]
}

export const TopNavigationComponent: React.FC<IProps> =
    ({
        navigation,
        hasMenuAction,
        hasDefaultTitle,
        defaultAction,
        title,
        rightAccessories = [],
    }) => {
        useEffect(() => {
        }, [])

        const toggleDrawer = () => {
            navigation.dispatch(DrawerActions.toggleDrawer())
        }

        const TitleImage = () => (
            // <Image
            //     source={{
            //         uri: require('./ROAMR-03-image-title.png')
            //     }}
            // />
            <Image
                source={require('./ROAMR-03-image-title.png')}
                style={{
                    height: 25,
                    width: 150,
                }}
            />
        )

        const TitleAction = () => (
            <TopNavigationAction
                // icon={MenuIcon}
                icon={TitleImage}
                onPress={defaultAction}
            />
        )

        const MenuAction = () => (
            <TopNavigationAction
                icon={MenuIcon}
                onPress={toggleDrawer}
            />
        )

        const renderAction = (item: IAccessory, index: number) => {
            if (item !== undefined) {
                return (
                    <TopNavigationAction
                        key={index}
                        icon={item.icon}
                        onPress={item.action}
                    />
                )
            } else {
                return <></>
            }
        }

        const renderLeftActions: RenderProp<{}> = () => {
            return (
                <>
                    {hasMenuAction ? <MenuAction /> : <></>}
                    {hasDefaultTitle ? <TitleAction /> : <></>}
                </>
            )
        }

        const renderRightActions: RenderProp<{}> = () => {
            return (
                <>
                    {rightAccessories.map((item, i) => (
                        renderAction(item, i)
                    ))}
                </>
            )
        }

        return (
            <>
                <TopNavigation
                    title={hasDefaultTitle ? '' : title}
                    alignment='center'
                    accessoryLeft={renderLeftActions}
                    accessoryRight={renderRightActions}
                />
                <Divider />
            </>
        )
    }