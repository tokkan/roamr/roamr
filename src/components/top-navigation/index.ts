import { ImageProps } from "react-native"

export interface IAccessory {
    title?: string
    icon: (props?: Partial<ImageProps> | undefined) => React.ReactElement<ImageProps>
    action: (...params: any) => void
}