import React, { useState, useEffect } from 'react'
import { Button, Text } from '@ui-kitten/components'
import { Animated, StyleProp, ViewStyle, ImageProps, EasingFunction } from 'react-native'
import { RenderProp } from '@ui-kitten/components/devsupport'
import { from, zip, of } from 'rxjs'
import { groupBy, mergeMap, toArray } from 'rxjs/operators'

const AnimatedButton = Animated.createAnimatedComponent(Button)

interface IActionGroups {
    action: IAnimatedComponent[]
}

interface IAnimatedComponent {
    style: StyleProp<ViewStyle>
    accessoryLeft: RenderProp<Partial<ImageProps>> | undefined
    onPress: any
    openGroup: number | null
    group_index: number
    text?: string
}

interface IProps {
    yIsTop: boolean,
    xIsLeft: boolean,
    initialPosition: {
        x: number,
        y: number,
    },
    positionIncrementX: number,
    positionIncrementY: number,
    duration: number,
    staggerDelay: number,
    easing: EasingFunction,
    components: IAnimatedComponent[],
    dialIcon: RenderProp<Partial<ImageProps>> | undefined,
}

export const GroupedSpeedDialAbsolute: React.FC<IProps> = ({
    yIsTop,
    xIsLeft,
    initialPosition,
    positionIncrementX,
    positionIncrementY,
    duration,
    staggerDelay,
    easing,
    components,
    dialIcon,
}) => {
    const [isOpen, setIsOpen] = useState(false)
    const [showText, setShowText] = useState(false)
    const [dockPosProps, setDockPosProps] = useState({})
    const [groupIndex, setGroupIndex] = useState(0)
    const [useNativeDriver] = useState(false)

    // TODO: Add rotating animation to the dial icon/button

    const [animatedPositions, setAnimatedPositions] =
        useState<{
            pos: Animated.ValueXY,
            component: IAnimatedComponent
        }[]>(
            components.map(m => {
                return {
                    pos: new Animated.ValueXY({
                        x: initialPosition.x,
                        y: initialPosition.y,
                    }),
                    component: m
                }
            }))

    useEffect(() => {

        let dockPosProps = {}
        if (yIsTop) {
            dockPosProps = {
                // ...posProps,
                top: initialPosition.y,
            }
        } else {
            dockPosProps = {
                bottom: initialPosition.y
            }
        }

        if (xIsLeft) {
            dockPosProps = {
                ...dockPosProps,
                left: initialPosition.x
            }
        } else {
            dockPosProps = {
                ...dockPosProps,
                right: initialPosition.x
            }
        }
        setDockPosProps(dockPosProps)

        setAnimatedPositions(
            components.map(m => {
                return {
                    pos: new Animated.ValueXY({
                        x: initialPosition.x,
                        y: initialPosition.y,
                    }),
                    component: m
                }
            })
        )
    }, [])

    const staggerAnimationsRevert = () => {
        let animations = animatedPositions.map((m) => {
            return Animated.timing(
                m.pos,
                {
                    toValue: initialPosition,
                    duration: duration,
                    easing: easing,
                    useNativeDriver: false,
                }
            )
        })
        return animations
        // Animated.stagger(staggerDelay, animations).start()
    }

    const staggerAnimations = () => {
        let temp = animatedPositions.filter(f => {
            if (f.component.group_index === groupIndex + 1) {
                return f
            }
        })

        let animations = temp
            // .reverse()
            // .sort((a, b) => a - b)
            .map((m, i) => {
                return Animated.timing(
                    m.pos,
                    {
                        toValue: {
                            x: initialPosition.x + (positionIncrementX * (i + 1)),
                            y: initialPosition.y + (positionIncrementY * (i + 1))
                        },
                        duration: duration,
                        useNativeDriver,
                    }
                )
            })
        return animations
        // Animated.stagger(staggerDelay, animations).start()
    }

    const staggerAnimationsReverse = (group_index: number) => {
        // TODO: Sort depending on direction
        let temp = animatedPositions.filter(f => {
            // if (f.component.group_index === groupIndex + 1) {
            if (f.component.group_index === group_index) {
                return f
            }
        })

        let animations = []
        for (let i = temp.length - 1; i >= 0; i--) {
            const m = temp[i]

            animations.push(Animated.timing(
                m.pos,
                {
                    toValue: {
                        x: initialPosition.x + (positionIncrementX * (i + 1)),
                        y: initialPosition.y + (positionIncrementY * (i + 1))
                    },
                    duration: duration,
                    easing: easing,
                    useNativeDriver,
                }
            ))
        }
        return animations
        // Animated.stagger(staggerDelay, animations).start()
    }

    const toggle = () => {
        if (isOpen) {
            // if (groupIndex <= 0) {
            setIsOpen(false)
            setShowText(false)
            const animations = staggerAnimationsRevert()
            Animated.stagger(staggerDelay, animations).start()
        } else {
            setIsOpen(!isOpen)
            setShowText(true)
            const animations = staggerAnimationsReverse(1)
            Animated.stagger(staggerDelay, animations).start()
            console.log('opening speed dial')
            console.log('groupIndex:')
            console.log(groupIndex)
        }
    }

    const toggleGroup = (group_index: number) => {
        console.log('toggleGroup')
        console.log(group_index)
        console.log(groupIndex)

        let revert = staggerAnimationsRevert()
        // Animated.stagger(staggerDelay, revert).start()
        let animations = staggerAnimationsReverse(group_index)

        Animated.sequence([
            Animated.stagger(staggerDelay, revert),
            // Animated.stagger(staggerDelay, animations),
            Animated.parallel(animations)
        ]).start()
    }

    return (
        <>
            {animatedPositions.map((m, i) => {
                let posProps = {}
                let textProps = {}
                if (yIsTop) {
                    posProps = {
                        // ...posProps,
                        top: m.pos.y,
                    }
                } else {
                    posProps = {
                        bottom: m.pos.y
                    }
                }

                if (xIsLeft) {
                    posProps = {
                        ...posProps,
                        left: m.pos.x
                    }
                    textProps = {
                        ...posProps,
                        left: initialPosition.x + 50
                    }
                } else {
                    posProps = {
                        ...posProps,
                        right: m.pos.x
                    }
                    textProps = {
                        ...posProps,
                        right: initialPosition.x + 50
                    }
                }

                return (
                    <>
                        {showText &&
                            m.component.text !== undefined &&
                            m.component.group_index === groupIndex &&
                            <Animated.View
                                key={i + 100}
                                style={[
                                    m.component.style,
                                    {
                                        flexDirection: 'column',
                                        justifyContent: 'center',
                                        backgroundColor: 'rgba(0, 0, 0, 0.1)',
                                        ...posProps,
                                        ...textProps,
                                    }
                                ]}
                            >
                                <Text style={{
                                    textAlign: 'center',
                                    textAlignVertical: 'center',
                                }}>
                                    {m.component.text}
                                </Text>
                            </Animated.View>
                        }
                        <AnimatedButton
                            key={i}
                            style={[
                                m.component.style,
                                {
                                    ...posProps,
                                }
                            ]}
                            // appearance='outline'
                            accessoryLeft={m.component.accessoryLeft}
                            onPress={() => {
                                // toggle()
                                // toggle()
                                // toggleGroup(m.component.group_index+1)
                                if (m.component.openGroup !== null) {
                                    setGroupIndex(m.component.openGroup)
                                    toggleGroup(m.component.openGroup)
                                } else {
                                    // toggleGroup(0)
                                    toggle()
                                    setGroupIndex(0)
                                    m.component.onPress()
                                }
                            }}
                        />
                    </>
                )
            })}
            <Button style={{
                position: 'absolute',
                elevation: 10,
                ...dockPosProps,
                borderRadius: 500,
                height: 50,
                width: 50,
                backgroundColor: '#222B45',
                borderColor: '#222B45',
            }}
                // appearance='outline'
                accessoryLeft={dialIcon}
                onPress={() => {
                    if (isOpen) {
                        toggle()
                        setGroupIndex(0)
                    } else {
                        setGroupIndex(1)
                        toggle()
                    }
                }}
            />
        </>
    )
}