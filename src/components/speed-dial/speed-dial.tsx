import React, { useState, useEffect } from 'react'
import { Button } from '@ui-kitten/components'
import { Animated, StyleProp, ViewStyle, ImageProps, Easing, EasingStatic, EasingFunction } from 'react-native'
import { RenderProp } from '@ui-kitten/components/devsupport'

const AnimatedButton = Animated.createAnimatedComponent(Button)

interface IAnimatedComponent {
    style: StyleProp<ViewStyle>
    accessoryLeft: RenderProp<Partial<ImageProps>> | undefined
    onPress: any
}

interface IProps {
    yIsTop: boolean,
    xIsLeft: boolean,
    initialPosition: {
        x: number,
        y: number,
    },
    positionIncrementX: number,
    positionIncrementY: number,
    duration: number,
    staggerDelay: number,
    easing: EasingFunction,
    components: IAnimatedComponent[],
    dialIcon: RenderProp<Partial<ImageProps>> | undefined,
}

export const SpeedDialAbsolute: React.FC<IProps> = ({
    yIsTop,
    xIsLeft,
    initialPosition,
    positionIncrementX,
    positionIncrementY,
    duration,
    staggerDelay,
    easing,
    components,
    dialIcon,
}) => {
    const [isOpen, setIsOpen] = useState(false)
    const [dockPosProps, setDockPosProps] = useState({})

    // TODO: Add rotating animation to the dial icon/button

    const [animatedPositions, setAnimatedPositions] =
        useState<{
            pos: Animated.ValueXY,
            component: IAnimatedComponent
        }[]>(
            components.map(m => {
                return {
                    pos: new Animated.ValueXY({
                        x: initialPosition.x,
                        y: initialPosition.y,
                    }),
                    component: m
                }
            }))

    useEffect(() => {

        let dockPosProps = {}
        if (yIsTop) {
            dockPosProps = {
                // ...posProps,
                top: initialPosition.y,
            }
        } else {
            dockPosProps = {
                bottom: initialPosition.y
            }
        }

        if (xIsLeft) {
            dockPosProps = {
                ...dockPosProps,
                left: initialPosition.x
            }
        } else {
            dockPosProps = {
                ...dockPosProps,
                right: initialPosition.x
            }
        }
        setDockPosProps(dockPosProps)

        setAnimatedPositions(
            components.map(m => {
                return {
                    pos: new Animated.ValueXY({
                        x: initialPosition.x,
                        y: initialPosition.y,
                    }),
                    component: m
                }
            })
        )
    }, [])

    const toggle = () => {
        if (isOpen) {
            setIsOpen(false)

            let animations = animatedPositions.map((m) => {
                return Animated.timing(
                    m.pos,
                    {
                        toValue: initialPosition,
                        duration: duration,
                        easing: easing,
                        useNativeDriver: false,
                    }
                )
            })

            Animated.stagger(staggerDelay, animations).start()
        } else {
            setIsOpen(!isOpen)

            // TODO: Sort depending on direction
            let temp = animatedPositions
            // if (initialPosition.y > positionIncrementY) {
            // temp = temp.reverse()
            // }

            let animations = []
            for (let i = temp.length - 1; i >= 0; i--) {
                const m = temp[i]
                animations.push(Animated.timing(
                    m.pos,
                    {
                        toValue: {
                            x: initialPosition.x + (positionIncrementX * (i + 1)),
                            y: initialPosition.y + (positionIncrementY * (i + 1))
                        },
                        duration: duration,
                        useNativeDriver: false,
                    }
                ))
            }

            // let animations = temp
            //     // .reverse()
            //     // .sort((a, b) => a - b)
            //     .map((m, i) => {
            //         return Animated.timing(
            //             m.pos,
            //             {
            //                 toValue: {
            //                     x: initialPosition.x + (positionIncrementX * (i + 1)),
            //                     y: initialPosition.y + (positionIncrementY * (i + 1))
            //                 },
            //                 duration: duration,
            //                 useNativeDriver: false,
            //             }
            //         )
            //     })

            Animated.stagger(staggerDelay, animations).start()
        }
    }

    return (
        <>
            {animatedPositions.map((m, i) => {
                let posProps = {}
                if (yIsTop) {
                    posProps = {
                        // ...posProps,
                        top: m.pos.y,
                    }
                } else {
                    posProps = {
                        bottom: m.pos.y
                    }
                }

                if (xIsLeft) {
                    posProps = {
                        ...posProps,
                        left: m.pos.x
                    }
                } else {
                    posProps = {
                        ...posProps,
                        right: m.pos.x
                    }
                }

                return (
                    <AnimatedButton
                        key={i}
                        style={[
                            m.component.style,
                            {
                                ...posProps,
                            }
                        ]}
                        accessoryLeft={m.component.accessoryLeft}
                        onPress={() => {
                            toggle()
                            m.component.onPress()
                        }}
                    />
                )
            })}
            <Button style={{
                position: 'absolute',
                elevation: 5,
                ...dockPosProps,
                borderRadius: 500,
                height: 50,
                width: 50,
                backgroundColor: 'green',
                borderColor: 'green',
            }}
                accessoryLeft={dialIcon}
                onPress={toggle}
            />
        </>
    )
}