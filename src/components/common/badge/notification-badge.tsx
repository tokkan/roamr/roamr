import React from 'react'
import { Text } from '@ui-kitten/components'
import { View } from 'react-native'

interface IProps {
    count: number
    color?: string
    textColor?: string
}

export const NotificationBadge: React.FC<IProps> = ({
    count,
    color = '#408000',
    textColor = 'white',
}) => {
    if (count > 0) {
        return (
            <View
                style={{
                    position: 'absolute',
                    height: 16,
                    width: 16,
                    // borderWidth: 1,
                    borderRadius: 200,
                    // borderBottomStartRadius: 200,
                    // borderBottomEndRadius: 200,
                    // borderBottomLeftRadius: 200,
                    // borderBottomRightRadius: 200,
                    // fontSize: 12,
                    top: 8,
                    left: 37,
                    // textAlign: 'center',
                    // color: textColor,
                    backgroundColor: color,
                }}
            >
                <Text
                    style={{
                        // position: 'absolute',
                        height: 16,
                        width: 16,
                        borderWidth: 1,
                        borderRadius: 200,
                        // borderBottomStartRadius: 200,
                        // borderBottomEndRadius: 200,
                        // borderBottomLeftRadius: 200,
                        // borderBottomRightRadius: 200,
                        fontSize: 12,
                        // top: 8,
                        // left: 37,
                        textAlign: 'center',
                        color: textColor,
                        // backgroundColor: color,
                    }}
                >
                    {count > 99 ?
                        99 : count}
                </Text>
            </View>
        )
    } else {
        return <></>
    }
}