import { Text, Toggle } from '@ui-kitten/components'
import React from 'react'
import { StyleSheet, View } from 'react-native'

const styles = StyleSheet.create({
    switchControl: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 10,
    }
})

interface ISwitchControlProps {
    text: string
    value: boolean
    handleValue: ((value: boolean) => void | undefined)
}

export const SwitchControl: React.FC<ISwitchControlProps> = ({
    text,
    value,
    handleValue,
}) => (
        <View
            style={styles.switchControl}
        >
            <Text>{text}</Text>
            <Toggle
                // style={{ marginLeft: 20, marginRight: 10 }}
                checked={value}
                onChange={handleValue}
            />
        </View>
    )