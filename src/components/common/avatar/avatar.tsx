import React, { FC, useEffect, useState } from 'react'
import { ImageProps } from 'react-native'
import { Avatar } from '@ui-kitten/components'

interface IProps {
    props: ImageProps
    source: string
}

export const DefaultAvatar: FC<IProps> = ({
    props,
    source,
}) => {
    const [defaultImage, setDefaultImage] = useState(false)
    const [randomInteger, setRandomInteger] = useState(0)
    const [defaultSrc, setDefaultSrc] = useState('')
    // const [defaultSource, setDefaultSource] = useState<NodeRequire>()

    useEffect(() => {
        setRandomInteger(random(1, 7))
        let int = random(1, 7)
        setDefaultSrc(`./default-avatar-${int}`)
        // setDefaultSource(require(`./default-avatar-${int}`))
    }, [])

    useEffect(() => {
        if (source !== undefined && source.length > 0) {
            setDefaultImage(true)
        }
    }, [source])

    const random = (min: number, max: number) => {
        return Math.floor(Math.random() * (max - min + 1) + min)
    }

    return (
        !defaultImage ?
            <Avatar
                {...props}
                defaultSource={require('./default-avatar-1.png')}
                source={require(`./default-avatar-4.png`)}
            />
            :
            <Avatar
                {...props}
                source={{
                    uri: source
                }}
            />
    )
}