import React, { useState } from 'react'
import { useTheme } from '@ui-kitten/components'
import { RefreshControl, ColorValue, RefreshControlProps } from 'react-native'

interface IProps {
    refreshing: boolean
    onRefresh: () => void
    tintColor?: string
    progressBackgroundColor?: string
    colors?: ColorValue[]
    size?: number
}

export const KittenRefreshControl: React.FC<IProps> = ({
    refreshing,
    onRefresh,
    tintColor,
    progressBackgroundColor,
    colors = undefined,
    size = undefined,
}): React.ReactElement<RefreshControlProps, string> => {
    const theme = useTheme()
    const [_tintColor] =
        useState(tintColor !== undefined ?
            tintColor : theme['primary-color'])
    // const [progressBackgroundColor] = useState('background-basic-color-1')
    const [_progressBackgroundColor] =
        useState(progressBackgroundColor !== undefined ?
            progressBackgroundColor : 'transparent')

    return (
        <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
            tintColor={_tintColor}
            progressBackgroundColor={_progressBackgroundColor}
            // colors={colors}
            // size={size}
        />
    )
}