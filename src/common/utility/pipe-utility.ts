/**
 * https://dev.to/ascorbic/creating-a-typed-compose-function-in-typescript-3-351i
 * @param fn1 
 * @param fns 
 */
export const pipe = <T extends any[], R>(
    fn1: (...args: T) => R,
    ...fns: Array<(a: R) => R>
) => {
    const piped = fns.reduce(
        (prevFn, nextFn) => (value: R) => nextFn(prevFn(value)),
        value => value
    );
    return (...args: T) => piped(fn1(...args));
};

export const valuePairsToObject = (obj: {
    [key: string]: any,
    prop: string,
    value: any,
}[]) => {
    return obj.reduce((acc, curr) => {
        return {
            ...acc,
            [curr.prop]: curr.value
        }
    }, {})
}

export const objectToValuePairs = (
    obj: any,
) => {
    let entries = Object.entries(obj)
    let pair = entries.reduce((prev: any, curr: any) => {
        return [
            ...prev,
            {
                prop: curr[0],
                value: curr[1],
            }
        ]
    }, [])
    return pair
}