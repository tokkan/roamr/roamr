export const isObjectNullOrUndefined = (obj: any) => {
    if (obj === null ||
        obj === undefined) {
        return false
    } else {
        return true
    }
}

export const isObjectEqual = (obj1: any, obj2: any) => {
    if (isObjectNullOrUndefined(obj1) ||
        isObjectNullOrUndefined(obj2)) {
        return false
    }

    const keys = Object.keys({ ...obj1, ...obj2 })
    let res = keys.every(e => obj1[e] === obj2[e])
    return res
}