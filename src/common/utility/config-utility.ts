import { getUniqueId } from 'react-native-device-info'
import messaging from '@react-native-firebase/messaging'

/**
 *
This is a constant and may be referenced directly

Gets the device unique ID. On Android it is currently identical to getAndroidId() in this module. On iOS it uses the DeviceUID uid identifier. On Windows it uses Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation.id.

Notes
iOS: This is IDFV or a random string if IDFV is unavaliable. Once UID is generated it is stored in iOS Keychain and NSUserDefaults. So it would stay the same even if you delete the app or reset IDFV. You can carefully consider it a persistent, cross-install unique ID. It can be changed only in case someone manually override values in Keychain/NSUserDefaults or if Apple would change Keychain and NSUserDefaults implementations. Beware: The IDFV is calculated using your bundle identifier and thus will be different in app extensions.
android: Prior to Oreo, this id (ANDROID_ID) will always be the same once you set up your phone.

 * iOS: "FCDBD8EF-62FC-4ECB-B2F5-92C9E79AC7F9"
 * Android: "dd96dec43fb81c97"
 * Windows: ?
 */
export const getDeviceUniqueId = () => {
    return getUniqueId()
}

export const getFirebaseDeviceToken = async () => {
    // messaging()
    //     .getToken()
    //     .then(token => {
    //         callback(token)
    //         // return saveTokenToDatabase(token);
    //     })
    return await messaging().getToken()
}