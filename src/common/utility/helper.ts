import { ColorSchemeName, Platform } from "react-native"
import changeNavigationBarColor, {
    showNavigationBar, hideNavigationBar
} from "react-native-navigation-bar-color"
import { ThemeTypes } from "../../store/settings/actions"

export const getApplicationTheme = (
    colorScheme: ColorSchemeName,
    theme: {
        system_theme: boolean
        user_theme: ThemeTypes
    }
) => {
    if (!theme.system_theme) {
        return theme.user_theme
    } else {
        if (colorScheme !== null &&
            colorScheme !== undefined &&
            colorScheme === 'dark') {
            return 'dark'
        } else {
            return 'light'
        }
    }
}

// export const setTransparencyInTheme = (
//     theme: ThemeTypes,
//     transparency: boolean,
// ) => {
//     if (transparency) {
//         if (Platform.OS === 'android') {
//             // changeNavigationBarColor(color, darkIcons, true)
//         }
//     }
//     getTheme() === 'dark' ? '#222B45' : '#FFFFFF'
// }

export const setNavigationVisibility = (visibility: 'show' | 'hide') => {
    if (Platform.OS === 'android') {
        if (visibility === 'show') {
            showNavigationBar()
        } else if (visibility === 'hide') {
            hideNavigationBar()
        }
    }
}

export const setAndroidNavigationTransparency = (
    color: 'translucent' | 'transparent',
    darkIcons: boolean,
) => {
    if (Platform.OS === 'android') {
        changeNavigationBarColor(color, darkIcons, true)
    }
}

export const setAndroidNavigationColor = (
    color: string,
    darkIcons: boolean,
) => {
    if (Platform.OS === 'android') {
        changeNavigationBarColor(color, darkIcons, true)
    }
}