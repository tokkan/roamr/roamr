import * as React from 'react'
import { NavigationContainerRef } from '@react-navigation/native'
import { NavigationActions } from '../../screens/routes'

// TODO: check navigation docs to fix isReadyRef so it can be used
export const isReadyRef = React.createRef()
export const navigationRef = React.createRef<NavigationContainerRef>()

export function navigate(payload: NavigationActions) {
    const {
        name,
        params,
    } = payload
    _navigate(name, params)
}

function _navigate(name: string, params: any) {
    if (navigationRef.current) {
        // Perform navigation if the app has mounted
        navigationRef.current.navigate(name, params);
    } else {
        console.debug('failed to navigate')
        // You can decide what to do if the app hasn't mounted
        // You can ignore this, or add these actions to a queue you can call later
    }
}

export function getCurrentRoute() {
    // if (isReadyRef.current && navigationRef.current) {
    if (navigationRef.current) {
        // Perform navigation if the app has mounted
        // return navigationRef.current.getRootState()
        return navigationRef.current.getCurrentRoute()
    } else {
        // You can decide what to do if the app hasn't mounted
        // You can ignore this, or add these actions to a queue you can call later
        console.debug('get current route else and null')
        // console.debug(isReadyRef.current)
        console.debug(navigationRef.current)
        return null
    }
}