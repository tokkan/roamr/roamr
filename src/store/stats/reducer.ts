import { AnyAction } from 'redux'
import { initState, IStatsState, StatsActions, StatsActionTypes } from './actions'

export default function (
    state: IStatsState = initState,
    action: StatsActionTypes): IStatsState {
    // action: AnyAction): ICredentialState {
    switch (action.type) {
        case StatsActions.INCREMENT_COUNT:
            switch (action.payload.type) {
                case 'notification':
                    return {
                        ...state,
                        notification_count: state.notification_count + action.payload.count,
                    }
                case 'instant_message':
                    return {
                        ...state,
                        instant_message_count: state.instant_message_count + action.payload.count,
                    }

                default:
                    return state
            }

        case StatsActions.RESET_COUNT:
            switch (action.payload.type) {
                case 'notification':
                    return {
                        ...state,
                        notification_count: 0,
                    }
                case 'instant_message':
                    return {
                        ...state,
                        instant_message_count: 0,
                    }

                default:
                    return state
            }

        case StatsActions.RESET_ALL:
            return initState

        default:
            return state
    }
}