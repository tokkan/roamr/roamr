export interface IStatsState {
    notification_count: number
    instant_message_count: number
}

export const initState: IStatsState = {
    notification_count: 0,
    instant_message_count: 0,
}

export enum StatsActions {
    INCREMENT_COUNT = 'increment_count',
    RESET_COUNT = 'reset_count',
    RESET_ALL = 'reset_all',
}

export interface IStatsActionDispatch {
    [StatsActions.INCREMENT_COUNT]: IIncrementCountPayload
    [StatsActions.RESET_COUNT]: IResetCountPayload
    [StatsActions.RESET_ALL]: null
}

interface IAction<T, P> {
    type: T
    payload: P
}

export type StatsActionTypes =
    IIncrementCountAction |
    IResetCountAction |
    IResetAllCountAction

type CountTypes =
    'notification' |
    'instant_message'

interface IIncrementCountPayload {
    type: CountTypes
    count: number
}
export interface IIncrementCountAction extends IAction<
    typeof StatsActions.INCREMENT_COUNT,
    IIncrementCountPayload
    > { }

interface IResetCountPayload {
    type: CountTypes
}
export interface IResetCountAction extends IAction<
    typeof StatsActions.RESET_COUNT,
    IResetCountPayload
    > { }

export interface IResetAllCountAction extends IAction<
    typeof StatsActions.RESET_ALL,
    null
    > { }