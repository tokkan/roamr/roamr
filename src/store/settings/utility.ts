import { ColorSchemeName } from "react-native"
import { IAppThemeState } from "./actions"

export const getApplicationTheme = (
    colorScheme: ColorSchemeName,
    app_theme: IAppThemeState
) => {
    if (!app_theme.use_system_theme) {
        return app_theme.theme
    } else {
        if (colorScheme !== null &&
            colorScheme !== undefined &&
            colorScheme === 'dark') {
            return 'dark'
        } else {
            return 'light'
        }
    }
}