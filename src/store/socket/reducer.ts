import { AnyAction } from 'redux'
import { initState, ISocketState, SocketActions, SocketActionTypes } from './actions'

export default function (
    state: ISocketState = initState,
    action: SocketActionTypes): ISocketState {
    // action: AnyAction): ICredentialState {
    switch (action.type) {
        case SocketActions.CONNECT_SOCKET:
            return action.payload

        case SocketActions.DISCONNECT_SOCKET:
            state.socket?.removeAllListeners()
            state.socket?.close()
            return initState

        default:
            return state
    }
}