import { SocketEventTypes } from "src/common/socket"

export interface ISocketState {
    socket: SocketIOClient.Socket | null
}

export const initState: ISocketState = {
    socket: null
}

export enum SocketActions {
    CONNECT_SOCKET = 'connect_socket',
    DISCONNECT_SOCKET = 'disconnect_socket',
    EMIT = 'emit_socket_event',
}

export interface ISocketActionDispatch {
    [SocketActions.CONNECT_SOCKET]: IConnectSocketPayload
    [SocketActions.DISCONNECT_SOCKET]: null
}

interface IAction<T, P> {
    type: T
    payload: P
}

export type SocketActionTypes =
    IConnectSocketAction |
    IDisconnectSocketAction

interface IConnectSocketPayload {
    socket: SocketIOClient.Socket
}
export interface IConnectSocketAction extends IAction<
    typeof SocketActions.CONNECT_SOCKET,
    IConnectSocketPayload
    > { }

export interface IDisconnectSocketAction extends IAction<
    typeof SocketActions.DISCONNECT_SOCKET,
    null
    > { }

interface ISocketEventPayload<T extends SocketEventTypes, P> {
    event: T
    payload: P
}

interface ISocketEmitSendMessagePayload extends ISocketEventPayload<
    'send_message',
    {
        chat_id: string
        from: string
        message: string
    }
    > { }

export interface ISocketEmitAction extends IAction<
    typeof SocketActions.EMIT,
    ISocketEmitSendMessagePayload
    > { }

// interface ISetDeviceActionPayload {
//     device_id: string
// }
// export interface ISetDeviceAction extends IAction<
//     typeof SocketActions.SetDeviceId,
//     ISetDeviceActionPayload
//     > { }

// interface ISetNotificationPayload {
//     notification_token: string
// }
// export interface ISetNotificationAction extends IAction<
//     typeof SocketActions.SetNotificationToken,
//     ISetNotificationPayload
//     > { }

// interface ISetDevicePlatformPayload {
//     platform: typeof Platform.OS
// }
// export interface ISetDevicePlatformAction extends IAction<
//     typeof SocketActions.SetDevicePlatform,
//     ISetDevicePlatformPayload
//     > { }

// export interface IResetCredentials extends IAction<
//     typeof SocketActions.ResetCredentials,
//     null
//     > { }
