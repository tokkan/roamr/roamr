import { Platform } from 'react-native'

export interface ICredentialState {
    device_id: string
    notification_token: string
    platform: typeof Platform.OS | ''
}

export const initState: ICredentialState = {
    device_id: '',
    notification_token: '',
    platform: '',
}

export enum CredentialActions {
    SetCredentials = 'set credentials',
    SetDeviceId = 'set device id',
    SetNotificationToken = 'set notification token',
    SetDevicePlatform = 'set device platform',
    ResetCredentials = 'reset credentials'
}

export interface ICredentialActionDispatch {
    [CredentialActions.SetCredentials]: ISetCredentialsPayload
    [CredentialActions.ResetCredentials]: null
    [CredentialActions.SetDeviceId]: ISetDeviceActionPayload
    [CredentialActions.SetDevicePlatform]: ISetDevicePlatformPayload
    [CredentialActions.SetNotificationToken]: ISetNotificationPayload
}

interface IAction<T, P> {
    type: T
    payload: P
}

export type CredentialActionTypes =
    ISetCredentials |
    IResetCredentials |
    ISetDeviceAction |
    ISetNotificationAction |
    ISetDevicePlatformAction

interface ISetCredentialsPayload {
    device_id: string
    notification_token: string
    platform: typeof Platform.OS
}
export interface ISetCredentials extends IAction<
    typeof CredentialActions.SetCredentials,
    ISetCredentialsPayload
    > { }

interface ISetDeviceActionPayload {
    device_id: string
}
export interface ISetDeviceAction extends IAction<
    typeof CredentialActions.SetDeviceId,
    ISetDeviceActionPayload
    > { }

interface ISetNotificationPayload {
    notification_token: string
}
export interface ISetNotificationAction extends IAction<
    typeof CredentialActions.SetNotificationToken,
    ISetNotificationPayload
    > { }

interface ISetDevicePlatformPayload {
    platform: typeof Platform.OS
}
export interface ISetDevicePlatformAction extends IAction<
    typeof CredentialActions.SetDevicePlatform,
    ISetDevicePlatformPayload
    > { }

export interface IResetCredentials extends IAction<
    typeof CredentialActions.ResetCredentials,
    null
    > { }
