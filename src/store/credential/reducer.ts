import { AnyAction } from 'redux'
import { CredentialActions, CredentialActionTypes, ICredentialState, initState } from './actions'

export default function (
    state: ICredentialState = initState,
    action: CredentialActionTypes): ICredentialState {
    // action: AnyAction): ICredentialState {
    switch (action.type) {
        case CredentialActions.SetCredentials:
            return action.payload

        case CredentialActions.ResetCredentials:
            return initState

        case CredentialActions.SetDeviceId:
            return {
                ...state,
                device_id: action.payload.device_id
            }

        case CredentialActions.SetDevicePlatform:
            return {
                ...state,
                device_id: action.payload.platform
            }

        case CredentialActions.SetNotificationToken:
            return {
                ...state,
                device_id: action.payload.notification_token
            }

        default:
            return state
    }
}