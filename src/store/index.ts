import { combineReducers, createStore, applyMiddleware } from 'redux'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { persistStore, persistReducer } from 'redux-persist'
import createSagaMiddleware from 'redux-saga'
import autoMergeLevel2 from 'redux-persist/es/stateReconciler/autoMergeLevel2'
import { logoutSaga } from './auth/sagas'
import authReducer from './auth/auth-reducer'
import settingsReducer from './settings/reducer'
import credentialReducer from './credential/reducer'
import socketReducer from './socket/reducer'
import statsReducer from './stats/reducer'

const rootReducer = combineReducers({
    auth: authReducer,
    settings: settingsReducer,
    notification_credentials: credentialReducer,
    socket: socketReducer,
    stats: statsReducer,
})

export type IRootState = ReturnType<typeof rootReducer>

export interface IActionDispatch<T> {
    <K extends keyof T>(action: { type: K, payload: T[K] }): () => T
}

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    // stateReconciler: autoMergeLevel2,
    whitelist: [
        // 'authReducer',
        'auth',
        'settings',
        'notification_credentials',
    ],
    // blacklist: [],
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

const sagaMiddleWare = createSagaMiddleware()

const store = createStore(
    persistedReducer,
    applyMiddleware(sagaMiddleWare)
)
sagaMiddleWare.run(logoutSaga)

let persistor = persistStore(store)

export {
    store,
    persistor,
}