import React, { FC } from 'react'
import { Layout, Text } from '@ui-kitten/components'
import { FlatList, RefreshControl } from 'react-native'
import { i18n } from '../../../locales/localization'

interface IProps {
    refreshing: boolean
    onRefresh: any
}

export const EmptyList: FC<IProps> = ({
    onRefresh,
    refreshing,
}) => {
    return (
        <FlatList
            style={{
                width: '100%',
                height: '100%',
            }}
            data={[{
                _id: '0001',
                title: i18n.t('timeline.no_results')
            }]}
            renderItem={() => {
                return (
                    <Layout
                        style={{
                            flexDirection: 'column',
                            marginTop: 50,
                            marginHorizontal: 30,
                        }}
                    >
                        <Text>
                            {i18n.t('timeline.no_results')}
                        </Text>
                    </Layout>
                )
            }}
            keyExtractor={item => item._id}
            refreshing={refreshing}
            refreshControl={
                <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                />
            }
        />
    )
}