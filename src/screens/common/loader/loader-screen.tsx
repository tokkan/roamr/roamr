import React from "react"
import { Layout, Spinner } from "@ui-kitten/components"

export const LoaderScreen = () => {
    return (
        <Layout
            style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
            }
            }
        >
            <Spinner size='giant' />
        </Layout>
    )
}