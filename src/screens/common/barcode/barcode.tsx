import React, { useRef, useEffect, useState } from 'react'
import { Layout } from '@ui-kitten/components'
import { RNCamera, BarCodeReadEvent } from 'react-native-camera'
import { TopNavigationComponent } from './../../../components/top-navigation/top-navigation-stack'
import { StackNavigationProp } from '@react-navigation/stack'
import { RootStackSearchRoutesParamList } from './../../search'
import BarcodeMask from 'react-native-barcode-mask'

type ScreenNavigationProp = StackNavigationProp<
    RootStackSearchRoutesParamList,
    'Barcode scanner'
>

interface IProps {
    navigation: ScreenNavigationProp
}

export const BarcodeScannerScreen: React.FC<IProps> = ({
    navigation,
}) => {
    const cameraRef = useRef(null)
    const [isScanning, setIsScanning] = useState(true)

    useEffect(() => {

        const focus = navigation.addListener('focus', () => {
            // setSource(axios.CancelToken.source())
            setIsScanning(true)
        })

        const unsubscribe = navigation.addListener('blur', () => {
            // source.cancel()
            setIsScanning(false)
        })

        return () => {
            focus
            unsubscribe
            // source.cancel()
        }
    }, [navigation])

    const onReadBarcode = (event: BarCodeReadEvent) => {
        if (isScanning) {
            console.log(onReadBarcode.name)
            console.log(event)
            // cameraRef.current = null
            setIsScanning(false)
            navigation.navigate('Barcode results', {
                barcode_event: event,
            })
        }
    }

    return (
        <Layout
            style={{
                flex: 1,
            }}
        >
            <TopNavigationComponent
                navigation={navigation}
                title=''
            />
            <RNCamera
                ref={cameraRef}
                style={{
                    flex: 1,
                    width: '100%',
                }}
                onBarCodeRead={event => onReadBarcode(event)}
            >
                <BarcodeMask
                    edgeColor={'yellow'}
                    showAnimatedLine={false}
                />
            </RNCamera>
        </Layout>
    )
}