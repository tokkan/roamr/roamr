import React from 'react'
import { RouteProp } from '@react-navigation/native'
import { RootStackSearchRoutesParamList } from 'src/views/search'
import { StackNavigationProp } from '@react-navigation/stack'
import { Layout, Text } from '@ui-kitten/components'
import { TopNavigationComponent } from './../../../components/top-navigation/top-navigation-stack'

type ScreenRouteProp = RouteProp<
    RootStackSearchRoutesParamList,
    'Barcode results'
>

type ScreenNavigationProp = StackNavigationProp<
    RootStackSearchRoutesParamList,
    'Barcode results'
>

interface IProps {
    route: ScreenRouteProp
    navigation: ScreenNavigationProp
}

export const BarcodeResultScreen: React.FC<IProps> = ({
    route,
    navigation,
}) => {
    const { barcode_event } = route.params

    return (
        <Layout
            style={{
                flex: 1,
            }}
        >
            <TopNavigationComponent
                navigation={navigation}
                title=''
                // replaceNavigate={() => navigation.navigate('Search')}
                replaceNavigate={() => navigation.popToTop()}
            />
            <Text>
                Barcode results
            </Text>
            <Text>
                {barcode_event.data}
            </Text>
        </Layout>
    )
}