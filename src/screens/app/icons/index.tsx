import React from 'react'
import { StyleSheet } from 'react-native'
// import Fa5IconBase from 'react-native-vector-icons/FontAwesome5'
import MaterialIconBase from 'react-native-vector-icons/MaterialIcons'
import MDIIconBase from 'react-native-vector-icons/MaterialCommunityIcons'

// Fa5IconBase.loadFont()
MaterialIconBase.loadFont()
MDIIconBase.loadFont()

function createIconsMap(provider: any) {
    return new Proxy({}, {
        get(target, name) {
            return provider(name);
        },
    })
}

// const IconFa5Provider = (name: string | number | symbol) => ({
//     toReactElement: (props: any) => FontAwesome5Icon({ name, ...props }),
// })

// function FontAwesome5Icon({ name, style }: any) {
//     const { height, tintColor, ...iconStyle } = StyleSheet.flatten(style);
//     return (
//         <Fa5IconBase name={name} size={height} color={tintColor} style={iconStyle} />
//     )
// }

const IconMaterialProvider = (name: string | number | symbol) => ({
    toReactElement: (props: any) => MaterialDesignIcon({ name, ...props }),
})

function MaterialDesignIcon({ name, style }: any) {
    const { height, tintColor, ...iconStyle } = StyleSheet.flatten(style);
    return (
        <MaterialIconBase name={name} size={height} color={tintColor} style={iconStyle} />
    )
}

const IconMDIProvider = (name: string | number | symbol) => ({
    toReactElement: (props: any) => MDIIcon({ name, ...props }),
})

function MDIIcon({ name, style }: any) {
    let flatten = StyleSheet.flatten(style);

    if (flatten !== undefined) {
        const { height, tintColor, ...iconStyle } = flatten
        return (
            <MDIIconBase name={name} size={height} color={tintColor} style={iconStyle} />
        )
    } else {
        return (
            <MDIIconBase name={name} size={12} />
        )
    }
}

// export const Fa5IconsPack = {
//     name: 'fa5',
//     icons: createIconsMap(IconFa5Provider),
// }

export const MaterialIconPack = {
    name: 'material',
    icons: createIconsMap(IconMaterialProvider),
}

export const MDICommunityPack = {
    name: 'mdi',
    icons: createIconsMap(IconMDIProvider)
}