import React from 'react'
import { Provider } from 'react-redux'
import { store, persistor } from './../../store'
import { PersistGate } from 'redux-persist/integration/react'
import { App } from './App'
// import { SplashScreen } from '../login/splash-screen'

export const AppContainer = () => (
    <Provider store={store}>
        <PersistGate
            // loading={<SplashScreen />}
            loading={null}
            persistor={persistor}
        >
            <App />
        </PersistGate>
    </Provider>
)