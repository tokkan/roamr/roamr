import React, { useEffect, useState } from 'react'
import { Platform, StatusBar, StyleSheet, useColorScheme } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components'
import * as eva from '@eva-design/eva'
import { EvaIconsPack } from '@ui-kitten/eva-icons'
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context'
import { useSelector, useDispatch } from 'react-redux'
import Config from 'react-native-config'
import changeNavigationBarColor, {
  hideNavigationBar,
  showNavigationBar,
} from 'react-native-navigation-bar-color'
// import {
//   getDeviceUniqueId, getFirebaseDeviceToken
// } from './../../common/utility/config-utility'
import { MaterialIconPack, MDICommunityPack } from './icons'
import { IRootState, IActionDispatch } from '../../store'
import { ICredentialActionDispatch } from '../../store/credential/actions'
import { ISocketActionDispatch } from '../../store/socket'
import { IStatsActionDispatch } from '../../store/stats/actions'
import { AuthenticationNavigator } from './authentication/authentication-navigator'
import { SplashScreen } from './authentication/splash-screen'
import { AppNavigator } from './navigators/app/app-navigator'
import {
  getApplicationTheme, setAndroidNavigationColor, setAndroidNavigationTransparency
} from '../../common/utility/helper'
import { navigationRef } from '../../common/navigation'

export function App() {
  const auth = useSelector((s: IRootState) => s.auth)
  const credentials = useSelector((s: IRootState) => s.notification_credentials)
  const dispatch = useDispatch<IActionDispatch<ICredentialActionDispatch>>()
  const dispatchSocket = useDispatch<IActionDispatch<ISocketActionDispatch>>()
  const userStats = useSelector((s: IRootState) => s.stats)
  const dispatchStats = useDispatch<IActionDispatch<IStatsActionDispatch>>()
  const app_theme = useSelector((s: IRootState) => s.settings.app_theme)
  const language = useSelector((s: IRootState) => s.settings.language)
  const colorScheme = useColorScheme()
  const [theme, setTheme] = useState(eva.dark)
  const [isLoading] = useState(false)

  useEffect(() => {
    setApplicationTheme()
  }, [app_theme])

  const getTheme = () => {
    return getApplicationTheme(colorScheme, {
      system_theme: app_theme.use_system_theme,
      user_theme: app_theme.theme,
    })
  }

  const setApplicationTheme = () => {
    const theme = getTheme()
    if (theme === 'dark') {
      setTheme(eva.dark)
      setAndroidNavigationTransparency("translucent", false)
    } else {
      setTheme(eva.light)
      setAndroidNavigationColor("#F7F9FC", true)
    }
  }

  if (isLoading) {
    return (
      <ApplicationProvider {...eva} theme={theme}>
        <SplashScreen />
      </ApplicationProvider>
    )
  } else {
    return (
      <ApplicationProvider {...eva} theme={theme}>
        <IconRegistry icons={[
          EvaIconsPack,
          // Fa5IconsPack,
          MaterialIconPack,
          MDICommunityPack,
        ]} />
        <SafeAreaProvider>
          <SafeAreaView
            style={{
              flex: 1,
              backgroundColor: getTheme() === 'dark' ? '#222B45' : '#FFFFFF',
            }}
          >
            <StatusBar
              // translucent={true}
              backgroundColor={getTheme() === 'dark' ? '#222B45' : '#FFFFFF'}
              barStyle={getTheme() === 'dark' ? 'light-content' : 'dark-content'}
            />
            <NavigationContainer
              ref={navigationRef}
            >
              {!auth.isAuthenticated ?
                <AuthenticationNavigator /> :
                <AppNavigator />
              }
            </NavigationContainer>
            {/* <FlashMessage
              position='top'
              // animationDuration={2000}
              // duration={5000}
              style={{
                top: 30,
                // paddingTop: 30,
              }}
            /> */}
          </SafeAreaView>
        </SafeAreaProvider>
      </ApplicationProvider >
    )
  }
}
