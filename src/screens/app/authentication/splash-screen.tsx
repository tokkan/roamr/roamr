import React from 'react'
import { StyleSheet, ImageProps } from 'react-native'
import { Button, Layout, Text, Icon } from '@ui-kitten/components'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        textAlign: 'center',
    },
    likeButton: {
        marginVertical: 16,
    },
})

export const SplashScreen = () => {

    return (
        <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Layout style={styles.container}>
                <Text style={styles.text} category='h1'>
                    Kappa Online
                </Text>
            </Layout>
        </Layout>
    )
}