export interface IFieldValidation {
    usernameMinLength: number,
    playerNameMinLength: number,
    passwordMinLength: number,
}

export const initFieldValidation: IFieldValidation = {
    usernameMinLength: 3,
    playerNameMinLength: 3,
    passwordMinLength: 6,
}