import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { LoginScreen } from './login/login'
import { RegisterScreen } from './register/register'
import { RootStackLoginRoutesParamList } from '../../routes'

const Stack = createStackNavigator<RootStackLoginRoutesParamList>()

export const LoginNavigator = () => {
    return (
        <Stack.Navigator
            initialRouteName={'Login'}
            headerMode='none'
        >
            <Stack.Screen
                name={'Login'}
                component={LoginScreen}
            />
            <Stack.Screen
                name={'Register'}
                component={RegisterScreen}
            />
        </Stack.Navigator>
    )
}

export const AuthenticationNavigator = () => {
    return (
        <LoginNavigator />
    )
}