import React, { useState } from 'react'
import { Layout, Text, Input, Toggle, Button, Spinner, Icon } from '@ui-kitten/components'
import { View } from 'react-native'
import { Formik } from 'formik'
import {
    object as yupobject,
    string as yupstring,
    boolean as bool,
} from 'yup'
import { StackNavigationProp } from '@react-navigation/stack'
import { RouteProp } from '@react-navigation/native'
import { useDispatch } from 'react-redux'
import { getNotificationCredentials, registerSocialAccount, socialLogin } from '../login/login-service'
import { i18n } from '../../../../locales/localization'
import { UserIcon } from '../../../../models'
import { IActionDispatch } from '../../../../store'
import { IAuthActionDispatch, AuthActions } from '../../../../store/auth'
import { RootStackLoginRoutesParamList } from '../../../routes'

interface IStateFields {
    username: string
    use_fb_avatar: boolean
    use_real_name: boolean
    private_account: boolean
}

const initStateFields: IStateFields = {
    username: '',
    use_fb_avatar: true,
    use_real_name: true,
    private_account: false,
}

const validationSchema = yupobject().shape<IStateFields>({
    username: yupstring()
        .min(3)
        .max(25)
        .required(),
    use_fb_avatar: bool()
        .required(),
    use_real_name: bool()
        .required(),
    private_account: bool()
        .required()
})

type ScreenRouteProp = RouteProp<
    RootStackLoginRoutesParamList,
    'Register'
>

type ScreenNavigationProp = StackNavigationProp<
    RootStackLoginRoutesParamList,
    'Register'
>

interface IProps {
    route: ScreenRouteProp,
    navigation: ScreenNavigationProp,
}

export const RegisterScreen: React.FC<IProps> = ({
    route,
    navigation,
}) => {
    const {
        hasRealName,
        credential,
        facebookCredential,
        first_name,
        last_name
    } = route.params
    const dispatch = useDispatch<IActionDispatch<IAuthActionDispatch>>()
    const [isSubmitting, setIsSubmitting] = useState(false)

    const onSubmit = async (values: IStateFields) => {
        const {
            device_id,
            notification_token,
            platform,
        } = await getNotificationCredentials()
        let photoURL = credential.user.photoURL !== null ?
            credential.user.photoURL : ''
        try {
            await registerSocialAccount(
                credential.user.uid,
                credential.user.email,
                facebookCredential.token,
                values.username,
                first_name,
                last_name,
                '',
                values.use_fb_avatar ?
                    photoURL : '',
                values.private_account,
                values.use_real_name,
                device_id,
                notification_token,
                platform,
            )

            let response =
                await socialLogin(
                    credential.user.uid,
                    facebookCredential.token,
                    'facebook',
                    device_id,
                    notification_token,
                    platform,
                )

            if (response) {
                setIsSubmitting(false)

                dispatch({
                    type: AuthActions.SOCIAL_LOGIN,
                    payload: {
                        _id: response.data._id,
                        id: credential.user.uid,
                        username: values.username,
                        avatar: credential.user.photoURL || '',
                        name: credential.user.displayName || '',
                        first_name,
                        last_name,
                        access_token: response.data.access_token,
                        social_provider: 'facebook',
                        facebook_access_token: facebookCredential.token,
                        mapbox_access_token: response.data.mapbox_access_token,
                        roles: response.data.roles,
                    }
                })

            } else {
                setIsSubmitting(false)
                // source.cancel('Operation cancelled by failing getting response')
            }
        } catch (error) {
            setIsSubmitting(false)
            navigation.goBack()
        }
    }

    const LoadingIndicator = (props: any) => (
        <View style={[props.style, {
            justifyContent: 'center',
            alignItems: 'center',
        }]}>
            {isSubmitting ?
                <Spinner size='small' /> :
                <Icon {...props} name='person-add-outline' />
            }
        </View>
    )

    return (
        <Layout
            style={{
                flex: 1,
                flexDirection: 'column',
                paddingTop: 70,
                paddingHorizontal: 10,
            }}
        >
            <Formik
                initialValues={initStateFields}
                validationSchema={validationSchema}
                onSubmit={values => onSubmit(values)}
                initialErrors={{
                    username: undefined
                }}
            >
                {({
                    values,
                    errors,
                    isValid,
                    touched,
                    handleChange,
                    setFieldValue,
                    handleBlur,
                    handleReset,
                    handleSubmit,
                }) => (
                        <>
                            <Text
                                category='h3'
                            >
                                Register
                            </Text>
                            <Input
                                style={{
                                    marginTop: 20,
                                }}
                                label='Username'
                                placeholder='Username'
                                size='medium'
                                accessoryRight={UserIcon}
                                value={values.username}
                                onChangeText={handleChange('username')}
                            />
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    marginTop: 20,
                                }}
                            >
                                <View
                                    style={{
                                        flexDirection: 'column',
                                        maxWidth: '70%',
                                    }}
                                >
                                    <Text
                                        style={{
                                            textAlignVertical: 'center',
                                        }}
                                    >
                                        Use your Facebook avatar
                                    </Text>
                                    <Text
                                        appearance='hint'
                                    >
                                        The avatar can be changed later
                                    </Text>
                                </View>
                                <View>
                                    <Toggle
                                        checked={values.use_fb_avatar}
                                        onChange={e => setFieldValue('use_fb_avatar', e)}
                                    />
                                </View>
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    marginTop: 20,
                                }}
                            >
                                <View
                                    style={{
                                        flexDirection: 'column',
                                        maxWidth: '70%',
                                    }}
                                >
                                    <Text
                                        style={{
                                            textAlignVertical: 'center',
                                        }}
                                    >
                                        Private account
                                    </Text>
                                    <Text
                                        appearance='hint'
                                    >
                                        Private account can be turned on and off in settings
                                    </Text>
                                </View>
                                <View>
                                    <Toggle
                                        checked={values.private_account}
                                        onChange={e => setFieldValue('private_account', e)}
                                    />
                                </View>
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    marginTop: 20,
                                }}
                            >
                                <View
                                    style={{
                                        flexDirection: 'column',
                                        maxWidth: '70%',
                                    }}
                                >
                                    <Text>
                                        Use real name
                                    </Text>
                                    <Text
                                        appearance='hint'
                                    >
                                        Use your name from Facebook
                                    </Text>
                                </View>
                                <View>
                                    <Toggle
                                        disabled={!hasRealName}
                                        checked={!hasRealName ? false : values.use_real_name}
                                        onChange={e => setFieldValue('use_real_name', e)}
                                    />
                                </View>
                            </View>
                            <View style={{
                                marginTop: 40,
                                flexDirection: 'row',
                                justifyContent: 'flex-end',
                            }}>
                                <Button
                                    style={[
                                        {
                                            marginRight: 20,
                                        }
                                    ]}
                                    appearance='outline'
                                    onPress={() => {
                                        handleReset()
                                    }}
                                >
                                    {i18n.t('common.reset')}
                                </Button>
                                <Button
                                    disabled={!isValid}
                                    appearance='outline'
                                    accessoryLeft={LoadingIndicator}
                                    onPress={() => handleSubmit(undefined)}
                                >
                                    {i18n.t('common.register')}
                                </Button>
                            </View>
                        </>
                    )}
            </Formik>
        </Layout>
    )
}