import axios from 'axios'
import { Platform } from 'react-native'
import { LoginManager, AccessToken } from 'react-native-fbsdk'
import auth from '@react-native-firebase/auth'
import {
    getDeviceUniqueId, getFirebaseDeviceToken
} from '../../../../common/utility/config-utility'

export async function accountExists(
    uid: string
) {
    try {
        let account_exists =
            await axios.post<boolean>('/auth/check-account',
                {
                    id: uid,
                    // user_id: uid,
                    // user_id: credential.user.uid,
                    // email: credential.user.email,
                    // // user_id: credential.user.uid,
                    // access_token: facebookCredential.token,
                })
        return account_exists.data
    } catch (error) {
        console.debug('error in auth/check-account endpoint:')
        console.debug(error)
        console.debug(axios.defaults)
        return false
    }
}

export async function registerSocialAccount(
    uid: string,
    email: string | null,
    access_token: string,
    username: string,
    first_name: string,
    last_name: string,
    avatar: string,
    avatar_url: string,
    private_account: boolean,
    use_real_name: boolean,
    device_id: string,
    notification_token: string,
    platform: typeof Platform.OS,
) {
    try {
        let social_response =
            await axios.post('/auth/register-from-social',
                {
                    // user_id: credential.user.uid,
                    // email: credential.user.email,
                    // access_token: facebookCredential.token,
                    user_id: uid,
                    email,
                    access_token,
                    username,
                    first_name,
                    last_name,
                    avatar,
                    avatar_url,
                    private_account,
                    use_real_name,
                    device_id,
                    notification_token,
                    platform,
                })

        if (!social_response.data.validation_passed) {
            throw 'Creation of user account failed'
        }
    } catch (error) {
        return null
    }
}

export async function socialLogin(
    uid: string,
    access_token: string,
    social_provider: 'facebook',
    device_id: string,
    notification_token: string,
    platform: typeof Platform.OS,
) {
    try {
        let response =
            await axios.post('/auth/social-login',
                {
                    // user_id: uid,
                    id: uid,
                    access_token,
                    social_provider,
                    device_id,
                    notification_token,
                    platform,
                },
                {
                    // cancelToken: source.token
                })
        return response
    } catch (error) {
        return null
    }
}

export async function facebookLogin() {
    // Attempt login with permissions
    const result =
        await LoginManager.logInWithPermissions(['public_profile', 'email'])

    if (result.isCancelled) {
        // throw 'User cancelled the login process'
        return null
    }

    // Once signed in, get the users AccessToken
    const data = await AccessToken.getCurrentAccessToken()

    if (!data) {
        // throw 'Something went wrong obtaining access token'
        return null
    }

    return data
}

export async function firebaseFacebookLogin(fb_access_token: AccessToken) {
    try {
        // Create a Firebase credential with the AccessToken    
        const facebookCredential =
            auth.FacebookAuthProvider.credential(fb_access_token.accessToken)

        // Sign-in the user with the credential
        let credential = await auth().signInWithCredential(facebookCredential)

        return {
            facebookCredential,
            credential,
        }
    } catch (error) {
        console.debug('error in firebase:')
        console.debug(error)
        return {
            facebookCredential: null,
            credential: null,
        }
    }
}

export async function logoutUser() {
    LoginManager.logOut()
    await auth().signOut()
}

export async function getNotificationCredentials() {
    let uid = getDeviceUniqueId()
    let fid = await getFirebaseDeviceToken()
    let platform = Platform.OS

    return {
        device_id: uid,
        notification_token: fid,
        platform,
    }
}