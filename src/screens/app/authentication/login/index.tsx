export interface IState extends IStateFields {
    showPassword: boolean
}

export interface IStateFields {
    username: string
    password: string
}

export interface IFieldValidation {
    usernameMinLength: number,
    playerNameMinLength: number,
    passwordMinLength: number,
}

export const initFieldValidation: IFieldValidation = {
    usernameMinLength: 3,
    playerNameMinLength: 3,
    passwordMinLength: 6,
}