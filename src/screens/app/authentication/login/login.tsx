import React, { useState, useEffect } from 'react'
import {
    Image, TouchableOpacity
} from 'react-native'
import {
    Button,
    Divider, Layout, Text
} from '@ui-kitten/components'
import { useDispatch } from 'react-redux'
import { NavigationProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import axios from 'axios'
import { IStateFields } from '.'
import { FacebookLoginComponent } from './fb-login-component'
import { accountExists, registerSocialAccount, socialLogin, facebookLogin, firebaseFacebookLogin, getNotificationCredentials, logoutUser } from './login-service'
import { RootStackLoginRoutesParamList } from '../../../routes'
import { IActionDispatch, persistor } from '../../../../store'
import { IAuthActionDispatch, AuthActions } from '../../../../store/auth'
import { roamrTextBlackBottom } from './../../../../models/images'

type ScreenNavigationProp = StackNavigationProp<
    RootStackLoginRoutesParamList,
    'Login'
>

interface IProps {
    // navigation: NavigationProp<any, any>
    navigation: ScreenNavigationProp
}

export const LoginScreen: React.FC<IProps> = ({ navigation }) => {
    const dispatch = useDispatch<IActionDispatch<IAuthActionDispatch>>()
    const [isAuthenticating, setIsAuthenticating] = useState(false)
    const [isFBAuthenticating, setIsFBAuthenticating] = useState(false)
    const [source, setSource] = useState(axios.CancelToken.source())

    useEffect(() => {
        const focus = navigation.addListener('focus', () => {
            setSource(axios.CancelToken.source())
        })

        const unsubscribe = navigation.addListener('blur', () => {
            source.cancel()
        })

        return () => {
            focus
            unsubscribe
            source.cancel()
        }
    }, [navigation])

    // const onLogin = async (values: IStateFields) => {
    //     try {
    //         setIsAuthenticating(true)

    //         console.log('onlogin')

    //         let response =
    //             await axios.post(LoginPaths.LOGIN,
    //                 {
    //                     username: values.username,
    //                     password: values.password
    //                 },
    //                 {
    //                     cancelToken: source.token
    //                 })

    //         console.log('onlogin response')
    //         console.log(response)

    //         if (response) {
    //             setIsAuthenticating(false)
    //             dispatch({
    //                 type: AuthActions.LOGIN,
    //                 payload: {
    //                     username: response.data.username,
    //                     access_token: response.data.access_token,
    //                 }
    //             })

    //         } else {
    //             setIsAuthenticating(false)
    //             source.cancel('Operation cancelled by failing getting response')
    //         }

    //     } catch (error) {
    //         setIsAuthenticating(false)

    //         if (axios.isCancel(error)) {
    //             if (__DEV__) {
    //                 console.debug('HTTP request cancelled', error.message)
    //             }
    //         }

    //         if (__DEV__) {
    //             console.debug(error)
    //         }
    //     }
    // }

    const resetState = async () => {
        await logoutUser()
        persistor.purge()
        dispatch({
            type: AuthActions.LOGOUT,
            payload: null
        })
    }

    const onFacebookLogin = async () => {
        setIsFBAuthenticating(true)

        const {
            device_id,
            notification_token,
            platform
        } = await getNotificationCredentials()

        let data = await facebookLogin()

        if (data === null) {
            setIsFBAuthenticating(false)
            return
        }

        const {
            facebookCredential,
            credential,
        } = await firebaseFacebookLogin(data)

        if (facebookCredential === null ||
            credential === null) {
            console.debug('Firebase facebook login failed')
            return
        }

        let first_name: string =
            credential.additionalUserInfo?.profile?.first_name || ''
        let last_name: string =
            credential.additionalUserInfo?.profile?.last_name || ''

        let account_exists =
            await accountExists(credential.user.uid)

        console.debug('account exists?')
        console.debug(account_exists)

        if (!account_exists) {
            const hasRealName = first_name === '' || last_name === '' ? false : true
            navigation.navigate('Register', {
                hasRealName,
                credential,
                facebookCredential,
                first_name,
                last_name,
            })
        }

        try {
            let response =
                await socialLogin(
                    credential.user.uid,
                    facebookCredential.token,
                    'facebook',
                    device_id,
                    notification_token,
                    platform,
                )

            if (response) {
                setIsFBAuthenticating(false)

                dispatch({
                    type: AuthActions.SOCIAL_LOGIN,
                    payload: {
                        _id: response.data._id,
                        id: credential.user.uid,
                        username: response.data.username,
                        avatar: credential.user.photoURL || '',
                        name: credential.user.displayName || '',
                        first_name: first_name,
                        last_name: last_name,
                        access_token: response.data.access_token,
                        social_provider: 'facebook',
                        facebook_access_token: facebookCredential.token,
                        mapbox_access_token: response.data.mapbox_access_token,
                        roles: response.data.roles,

                    }
                })

            } else {
                setIsFBAuthenticating(false)
                source.cancel('Operation cancelled by failing getting response')
            }
        } catch (error) {
            setIsFBAuthenticating(false)
            source.cancel()

            if (axios.isCancel(error)) {
                if (__DEV__) {
                    console.debug('HTTP request cancelled', error.message)
                }
            }

            if (__DEV__) {
                console.debug(error)
            }
        }
    }

    return (
        <Layout style={{
            flex: 1,
        }}>
            <Layout style={{
                paddingTop: 125,
                flexDirection: 'row',
                justifyContent: 'center',
            }}>
                <Image
                    style={{
                        height: 200,
                        resizeMode: 'contain',
                    }}
                    // source={require('./../../../images/roamr-with-text.png')}
                    source={roamrTextBlackBottom}
                />
            </Layout>
            <Layout style={{
                flex: 1,
                justifyContent: 'flex-start',
                paddingTop: 50,
            }}>
                <FacebookLoginComponent
                    isAuthenticating={isFBAuthenticating}
                    onLogin={onFacebookLogin}
                />
            </Layout>
            <Layout style={{
                flex: 1,
                justifyContent: 'flex-end',
                paddingTop: 20,
                paddingBottom: 30,
                paddingHorizontal: 50,
            }}>
                <Text
                    style={{
                        textAlign: 'center',
                    }}
                    appearance='hint'
                >
                    Info here
                </Text>
                <Text
                    style={{
                        marginTop: 10,
                        textAlign: 'center',
                    }}
                    appearance='hint'
                >
                    Terms of service link to website
                </Text>
            </Layout>
            <Layout
                level='2'
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    marginTop: 0,
                    paddingTop: 0,
                    paddingBottom: 10,
                    paddingHorizontal: 50,
                }}
            >
                <Text
                    style={{
                        textAlign: 'center',
                    }}
                    appearance='hint'
                >
                    If you want to login with a different facebook account clear your browser cache
                </Text>
                <TouchableOpacity
                    onPress={async () => await resetState()}
                >
                    <Text>
                        Reset state
                    </Text>
                </TouchableOpacity>
            </Layout>
            <Divider />
        </Layout>
    )
}