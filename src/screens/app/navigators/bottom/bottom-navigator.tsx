import React, { useEffect } from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { BottomNavigation, BottomNavigationTab, Divider } from '@ui-kitten/components'
import { UserIcon, HomeIcon, SearchIcon, MapIcon, NotificationsIcon, EventIcon } from '../../../../models'
import { useDispatch, useSelector } from 'react-redux'
import { IActionDispatch, IRootState } from '../../../../store'
import { IStatsActionDispatch, StatsActions } from '../../../../store/stats/actions'
import { ScreenRouteTypes } from '../../../routes/routes'
import { NotificationBadge } from '../../../../components/common/badge/notification-badge'
import { FeedNavigator } from '../../../social/feed/feed-navigator'
// import { HomeNavigator } from './../../../../screens/home/navigator/home-navigator'
// import { SearchNavigator } from './../../../../screens/search/free-search/search-navigator'
// import { MapNavigator } from './../../../../screens/map/map-navigator'
// import { UserNavigator } from './../../../../screens/profiles/user/user-navigator'
// import { EventNavigator } from './../../../social/event/event-navigator'

const { Navigator, Screen } =
    createBottomTabNavigator<Record<ScreenRouteTypes, object | undefined>>()

const BottomTabBar = ({ navigation, state }: any) => {
    const stats = useSelector((s: IRootState) => s.stats)
    const dispatch = useDispatch<IActionDispatch<IStatsActionDispatch>>()

    useEffect(() => {
        // TODO: Listen to store for notifications and messages, if they have been read/opened
        // TODO: dispatch reset of notification count in onPressIn below
        // TODO: To reset messages, the message screen should be opened, and also the conversation
    }, [])

    const resetNotifications = () => {
        dispatch({
            type: StatsActions.RESET_COUNT,
            payload: {
                type: 'notification'
            }
        })
    }

    const resetMessages = () => {
        dispatch({
            type: StatsActions.RESET_COUNT,
            payload: {
                type: 'instant_message'
            }
        })
    }

    return (
        <>
            <Divider />
            <BottomNavigation
                selectedIndex={state.index}
                onSelect={index => navigation.navigate(state.routeNames[index])}
            >
                <BottomNavigationTab icon={HomeIcon} />
                <BottomNavigationTab icon={SearchIcon} />
                <BottomNavigationTab icon={MapIcon} />
                {/* <BottomNavigationTab
                    title={props => <NotificationBadge
                        {...props}
                        count={stats.notification_count}
                    />}
                    icon={NotificationsIcon}
                    onPressIn={() => resetNotifications()}
                /> */}
                <BottomNavigationTab icon={EventIcon} />
                <BottomNavigationTab
                    title={props => <NotificationBadge
                        {...props}
                        count={stats.instant_message_count}
                    />}
                    icon={UserIcon}
                    onPressIn={() => resetMessages()}
                />
            </BottomNavigation>
        </>
    )
}

export const BottomNavigator = () => (
    <Navigator
        tabBar={(props: any) => <BottomTabBar {...props} />}
        initialRouteName='Home'
    >
        <Screen name={'Feed'} component={FeedNavigator} />
        {/* <Screen name={'Home'} component={HomeNavigator} />
        <Screen name={'Search'} component={SearchNavigator} />
        <Screen name={'Map'} component={MapNavigator} />
        <Screen name={'Notifications'} component={EventNavigator} />
        <Screen name={'User'} component={UserNavigator} /> */}
    </Navigator>
)