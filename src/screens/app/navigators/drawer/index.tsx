import { StyleSheet } from 'react-native'
import {
    IAuthActionDispatch
} from '../../../../store/auth'
import { IActionDispatch } from '../../../../store'
export const styles = StyleSheet.create({
    container: {
        backgroundColor: 'red',
        // flexDirection: 'column',
        // justifyContent: 'flex-end',
        // maxHeight: 320,
    },
    safeArea: {
        flex: 1,
    },
    header: {
        height: 128,
        // paddingHorizontal: 16,
        // justifyContent: 'center',
    },
    headerContainer: {
        height: 184
    },
    drawerHeaderContainer: {
        // flex: 1,
        // resizeMode: 'cover',
        // justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        // height: '100%',
        // marginLeft: 10,
        // marginBottom: 5,

        // justifyContent: 'flex-start',
        // alignContent: 'flex-start',
        // alignItems: 'flex-start',
        // alignSelf: 'flex-start',
        // backgroundColor: 'rgba(0, 0, 0, 0)'
    },
    drawerHeaderImageContainer: {
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center',
        // position: 'absolute',
        // top: 0,
        // left: 0,
        // height: 1000,
        // width: 1000,
        opacity: 0.3,
    },
    drawerInnerContainer: {
        // flex: 1,
        flexDirection: 'row',
        // justifyContent: 'flex-start',
        // backgroundColor: 'black',
    },
})

export interface IDrawerItem {
    // title: Routes & string
    title: string
    description: string
    leftAccessory: (props: any) => JSX.Element
    rightAccessory?: (props: any) => JSX.Element
    function: ([...args]: any) => void
    roles: string[]
}

export interface IDrawerItemAction extends IDrawerItem {
    function: ([...args]: any) => any
}

export interface IDrawerItemProps {
    index: number
    item: IDrawerItem
}

export interface IDrawerItemActionProps {
    index: number
    item: IDrawerItemAction
}

export interface IDrawerProps {
    dispatch: IActionDispatch<IAuthActionDispatch>
    navigation: any
    state: any
}