import React from 'react'
import { useDispatch } from 'react-redux'
import { createDrawerNavigator } from '@react-navigation/drawer'
import {
    IAuthActionDispatch
} from '../../../../store/auth'
import { IActionDispatch } from '../../../../store'
import { ScreenRouteTypes } from '../../../routes/routes'
import { AppDrawer } from '../drawer/app-drawer'
import { BottomNavigator } from '../bottom/bottom-navigator'
import { CreatePostNavigator } from '../../../social/post/create/create-post-navigator'

const { Navigator, Screen } =
    createDrawerNavigator<Record<ScreenRouteTypes, object | undefined>>()

export const AppNavigator: React.FC = () => {
    const dispatch = useDispatch<IActionDispatch<IAuthActionDispatch>>()

    return (
        <Navigator
            drawerContent={props =>
                <AppDrawer
                    dispatch={dispatch}
                    {...props}
                />
            }
            screenOptions={{
                headerShown: false,
            }}
        >
            <Screen name={'Home'} component={BottomNavigator} />
            <Screen name={'CreatePost'} component={CreatePostNavigator} />
            {/* <Screen name={'Search'} component={SearchNavigator} /> */}
            {/* <Screen name={'Map'} component={MapNavigator} /> */}
            {/* <Screen name={'Settings'} component={SettingsNavigator} /> */}
            {/* <Screen name={'Notifications'} component={NotificationNavigator} /> */}
            {/* <Screen name={'Messages'} component={MessagesNavigator} /> */}
        </Navigator>
    )
}