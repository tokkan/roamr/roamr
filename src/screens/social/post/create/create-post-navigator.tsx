import React from 'react'
import { createStackNavigator } from "@react-navigation/stack"
import { CreatePostScreen } from './screens/create-post-screen'
import { RootStackCreatePostRoutesParamList } from './../../../routes'
import { AVScreen } from '../../../../components/av/av-screen'
import { CameraPreviewScreen } from '../../../../components/av/preview/camera-preview-screen'

const Stack = createStackNavigator<RootStackCreatePostRoutesParamList>()

export const CreatePostNavigator = () => {
    return (
        <Stack.Navigator
            // initialRouteName={'Camera'}
            initialRouteName={'Camera'}
            headerMode='none'
        >
            <Stack.Screen
                name={'Camera'}
                component={AVScreen}
            />
            <Stack.Screen
                name={'CameraPreview'}
                component={CameraPreviewScreen}
            />
            <Stack.Screen
                name={'CreatePost'}
                component={CreatePostScreen}
            />
        </Stack.Navigator>
    )
}