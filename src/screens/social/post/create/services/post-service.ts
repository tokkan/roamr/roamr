import axios, { CancelToken } from 'axios'
import * as FileSystem from 'expo-file-system'
import { FileSystemUploadType } from 'expo-file-system'
import { PostedByTypes } from '../../../../../models/common/common'
import { IAuthHeaders } from '../../../../../models/common/network'

export interface IAssetPayload {
    index: number
    image_id: string
}

export interface ICreatePostPayload {
    poster_id: string,
    poster_user_id: string
    posted_by_type: PostedByTypes
    caption: string
    assets: IAssetPayload[]
    is_comments_enabled: boolean
    tagged_in_post: []
    is_location_enabled: boolean
    coordinates: number[]
}

export const createPost = async (
    // form_data: FormData,
    payload: ICreatePostPayload,
    headers: IAuthHeaders,
    cancelToken?: CancelToken,
) => {
    const {
        access_token,
        id,
        social_provider,
    } = headers
    try {
        let response =
            await axios.post<{
                index: number
                image_id: string
            }[]>('posts',
                // form_data,
                payload,
                {
                    headers: {
                        // 'Content-Type': 'multipart/form-data',
                        user_id: id,
                        access_token: access_token,
                        social_provider: social_provider,
                    },
                    cancelToken: cancelToken,
                })

        console.debug('got response')

        if (response) {
            return response.data
        }
        return null
    } catch (error) {
        console.debug('error')
        console.debug(error)
        return null
    }
}

interface IUploadAssetHeaders extends IAuthHeaders {
    index: number
    image_id: string
    // contentType: ContentTypes,
    asset_type: 'image/jpeg' | 'image/png' | 'image/gif' | 'video/mp4'
    content_type: string
}

// export const uploadAssetForPost = async (
//     source: string,
//     headers: IAuthHeaders,
// ) => {
//     try {
//         let buffer = await FileSystem.readAsStringAsync(source, {
//             encoding: FileSystem.EncodingType.UTF8,
//         })
//         let form_data = new FormData()
//         form_data.append('assets', buffer, 'post')
//         let response =
//             await axios.post('storage',
//                 form_data,
//                 {
//                     headers,
//                     responseType: 'arraybuffer',
//                 })
//     } catch (error) {
//         console.debug('error')
//         console.debug(error)
//     }
// }

export const uploadAssetForPost = async (
    source: string,
    headers: IUploadAssetHeaders,
) => {
    try {
        let buffer = await FileSystem.readAsStringAsync(source, {
            encoding: FileSystem.EncodingType.UTF8,
        })
        let file = await (await fetch(source)).blob()
        // let blob = await file.blob()

        let form_data = new FormData()
        // form_data.append('assets', buffer, 'post')
        // form_data.append('file', buffer, 'file')
        // form_data.append('file', JSON.stringify(buffer), 'file')
        // form_data.append('file', file)
        form_data.append('file', buffer)

        let response =
            await axios.post('storage',
                // form_data,
                // JSON.stringify(form_data),
                file,
                {
                    headers,
                    // headers: {
                    //     ...headers,
                    //     'Content-Type': 'multipart/form-data',
                    // },
                    // responseType: 'arraybuffer',
                    responseType: 'blob',
                })
        return response
    } catch (error) {
        console.debug('error')
        console.debug(error)
    }
}

export const uploadAssetFromPost = async (
    source: string,
    // headers: IAuthHeaders,
    headers: IUploadAssetHeaders,
    // params: {
    //     index: number
    //     id: string
    //     // contentType: ContentTypes,
    //     contentType: 'image/jpeg' | 'image/png' | 'image/gif' | 'video/mp4'
    // },
) => {
    // try {
    //     console.debug(uploadAssetFromPost.name)
    //     let _headers: Record<string, string> =
    //         Object.entries(headers)
    //             .reduce((prev, curr) => ({
    //                 ...prev,
    //                 [curr[0]]: curr[1]
    //             }), {})
    //     // let _headers: Record<string, string> = {
    //     //     [__headers[0][0]]: headers.id
    //     // }
    //     let headers_record: Record<string, string> = _headers

    //     console.debug(headers_record)
    //     console.debug(source)

    //     let res = await FileSystem.uploadAsync(
    //         `${axios.defaults.baseURL}/storage`,
    //         source,
    //         {
    //             httpMethod: 'POST',
    //             // fieldName: 'assets',
    //             // parameters: params,
    //             headers: headers_record,
    //             // uploadType: FileSystemUploadType.BINARY_CONTENT,
    //             // uploadType: FileSystemUploadType.MULTIPART,
    //         }
    //     )
    //     console.debug(res)
    // } catch (error) {
    //     console.debug('error')
    //     console.debug(error)
    // }

    try {
        console.debug(uploadAssetFromPost.name)
        let _headers: Record<string, string> =
            Object.entries(headers)
                .reduce((prev, curr) => ({
                    ...prev,
                    [curr[0]]: curr[1]
                }), {})
        let headers_record: Record<string, string> = _headers

        console.debug(headers_record)
        console.debug(source)

        let res = await FileSystem.uploadAsync(
            `${axios.defaults.baseURL}/storage`,
            source,
            {
                httpMethod: 'POST',
                // fieldName: 'assets',
                // fieldName: 'file',
                // parameters: params,
                headers: headers_record,
                // uploadType: FileSystemUploadType.BINARY_CONTENT,
                uploadType: FileSystemUploadType.MULTIPART,
            }
        )
        console.debug(res)
        return res
    } catch (error) {
        console.debug('error')
        console.debug(error)
        return null
    }
}

// export const createPostOneFile = async (
//     source: string,
//     headers: IAuthHeaders,
//     params: Record<string, string>,
// ) => {
//     try {
//         let res = await FileSystem.uploadAsync(
//             `${axios.defaults.baseURL}/posts`,
//             source,
//             {
//                 httpMethod: 'POST',
//                 // fieldName: 'assets',
//                 // parameters: params,
//                 headers,
//                 uploadType: FileSystemUploadType.BINARY_CONTENT,
//                 // uploadType: FileSystemUploadType.MULTIPART,
//             }
//         )
//         console.debug(res)
//     } catch (error) {
//         console.debug('error')
//         console.debug(error)
//     }
// }