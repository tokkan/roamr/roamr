import React, { FC, useCallback, useEffect, useState } from "react"
import { View, ScrollView } from "react-native"
import { Spinner, Icon, Layout, Input, Toggle, Button, Text, Select, IndexPath, SelectItem } from "@ui-kitten/components"
import { useSelector } from "react-redux"
import { RouteProp, useFocusEffect } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"
// import Geolocation, { GeolocationOptions, GeolocationResponse } from "@react-native-community/geolocation"
import * as Location from 'expo-location'
import * as FileSystem from 'expo-file-system'
import { Formik } from "formik"
// import { Image } from "react-native-image-crop-picker"
import { i18n } from "./../../../../../locales/localization"
import { InfoIcon } from "./../../../../../models"
import { styles } from "./../styles"
// import { ImagePreviewCarousel } from "./../components/image-preview-carousel"
// import { ImageModal } from "../components/image-modal"
// import { createPost } from "../../services/requests"
import { IRootState } from "./../../../../../store"
// import { askForLocationPermission, latLngToCoordinates } from "./../../../../../utils/geolocation/geolocation"
import { SwitchControl } from "./../../../../../components/common/switch-control/switch-control"
import {
    ICameraRouteParam, RootStackCreatePostRoutesParamList
} from "./../../../../routes"
import { createPost, ICreatePostPayload, uploadAssetForPost, uploadAssetFromPost } from "../services/post-service"
import { LocationObject } from "expo-location"
import { latLngToCoordinates } from "../../../../../common/geolocation"
import { PostedByTypes } from "../../../../../models/common/common"

interface IStateFields {
    caption: string
    disableLocation: boolean
    // // content: string
    // // description: string[]
    // // images: any[]
    // pin_type: PinTypesEnum
}

const initStateFields: IStateFields = {
    caption: '',
    disableLocation: false,
    // // content: '',
    // // description: [],
    // // images: [],
    // pin_type: PinTypesEnum.poi,
}

interface IFile {
    index: number
    image_id: string
    source: string
    type: string
    name: string
}

type ScreenRouteProp = RouteProp<
    RootStackCreatePostRoutesParamList,
    'CreatePost'
>

type ScreenNavigationProp = StackNavigationProp<
    RootStackCreatePostRoutesParamList,
    'CreatePost'
>

interface IProps {
    route: ScreenRouteProp
    navigation: ScreenNavigationProp
}

export const PostScreenComponent: FC<IProps> = ({
    route,
    navigation,
}) => {
    const auth = useSelector((s: IRootState) => s.auth)
    const [isSubmitting, setIsSubmitting] = useState(false)
    const [isImageModalVisible, setIsImageModalVisible] = useState(false)
    // const [assets, setAssets] = useState<Image[]>([])
    const [assets, setAssets] = useState<ICameraRouteParam[]>([])
    const [disableComments, setDisableComments] = useState(false)
    const [disableLocation, setDisableLocation] = useState(false)
    const [selectedProvider, setSelectedProvider] = useState(0)
    const [postedByIndex, setPostedByIndex] = useState(new IndexPath(0))
    const [postedByStaticChoices] = useState<{
        _id: string,
        name: string,
        type: PostedByTypes
    }[]>([
        {
            _id: auth._id,
            name: i18n.t('post.create.posted_by_user'),
            type: 'user',
        }
    ])
    const [postedByChoices, setPostedByChoices] = useState<{
        _id: string,
        name: string,
        type: PostedByTypes
    }[]>(postedByStaticChoices)
    const [selectedPostedBy, setSelectedPostedBy] =
        useState<{
            _id: string
            name: string
            type: PostedByTypes
        }>({
            _id: auth._id,
            name: i18n.t('post.create.posted_by_user'),
            type: 'user',
        })
    const [userPosition, setUserPosition] = useState<LocationObject>({
        coords: {
            accuracy: 0,
            altitude: 0,
            altitudeAccuracy: 0,
            heading: 0,
            latitude: 0,
            longitude: 0,
            speed: 0,
        },
        timestamp: 0,
    })
    const [files, setFiles] = useState<any[]>([])
    const [fileHandles, setFileHandles] = useState<IFile[]>([])

    useEffect(() => {
        getLocation()
        route.params.forEach(async f => {
            // console.debug('reading file...')
            // let file = await FileSystem.readAsStringAsync(f.source, {
            //     encoding: 'base64',
            // })
            // // let file = await FileSystem.readAsStringAsync(f.source, {
            // //     encoding: 'utf8',
            // // })
            // console.debug(file)
            // setFiles([...files, {
            //     source: file,
            //     type: f.type,
            //     name: f.name,
            // }])

            // TODO:
            // TODO:

            setFileHandles([
                ...fileHandles,
                ...route.params.map((m, i) => ({
                    index: i,
                    image_id: '',
                    source: m.source,
                    name: m.name,
                    type: m.type,
                }))
            ])
        })
    }, [])

    const getLocation = async () => {
        let { status } = await Location.requestPermissionsAsync()
        if (status !== 'granted') {
            console.debug(`Permissions for location wasn't granted`)
            return
        }

        let location = await Location.getCurrentPositionAsync({})
        setUserPosition(location)
    }

    // const [userPosition, setUserPosition] =
    //     useState<GeolocationResponse>({
    //         coords: {
    //             accuracy: 0,
    //             altitude: 0,
    //             altitudeAccuracy: 0,
    //             heading: 0,
    //             latitude: 0,
    //             longitude: 0,
    //             speed: 0,
    //         },
    //         timestamp: 0,
    //     })

    // useFocusEffect(
    //     useCallback(() => {

    //         console.debug('onFocusFX')
    //         console.debug(route)
    //         let params = route.params.map(m => m)
    //         setAssets(params)

    //         try {
    //             console.log('usefocusfx')
    //             let granted = askForLocationPermission()

    //             if (!granted) {
    //                 return
    //             }

    //             const geoLocationOptions: GeolocationOptions = {
    //                 timeout: 10000,
    //                 enableHighAccuracy: true,
    //                 maximumAge: undefined,
    //             }

    //             // setTimeout(() => {
    //             //     Geolocation.getCurrentPosition(
    //             //         (pos) => {
    //             //             setUserPosition(pos)
    //             //         },
    //             //         (err) => {
    //             //             console.debug('Failed to getCurrentPosition')
    //             //             console.debug(err)
    //             //         }, geoLocationOptions
    //             //     )
    //             // }, 2000)
    //         } catch (error) {
    //             console.debug(error)
    //         }
    //     }, [])
    // )

    const isFieldValid = (
        fieldValue: keyof IStateFields,
        errorValue: string | undefined,
        touched: boolean | undefined
    ) => {
        if (errorValue === undefined) {
            return true
        } else if (fieldValue.length > 0 && errorValue !== undefined) {
            return false
        } else {
            return false
        }
    }

    const setPostedBySelect = (index: any) => {
        setPostedByIndex(index)
        setSelectedPostedBy(postedByChoices[index - 1])
    }

    const onSubmit = async (values: IStateFields) => {
        try {
            setIsSubmitting(true)

            let form_data = new FormData()

            console.log('onSubmit')
            console.log(selectedPostedBy)

            const coordinates = latLngToCoordinates(
                userPosition.coords.latitude,
                userPosition.coords.longitude,
            )

            // console.debug('appending to form data')

            // // let assetsPayload: any[] = []
            // let assetsPayload: any[] = files
            // // console.debug(assetsPayload)

            // // form_data.append('assets', assetsPayload)

            // form_data.append('payload', JSON.stringify({
            //     poster_id: selectedPostedBy._id,
            //     poster_user_id: auth._id,
            //     posted_by_type: selectedPostedBy.type,
            //     caption: values.caption,
            //     // pin_type: pinTypeDisplayValue[1],
            //     assets: JSON.stringify(assetsPayload),
            //     // assets: assetsPayload,
            //     is_comments_enabled: !disableComments,
            //     tagged_in_post: [],
            //     is_location_enabled: !disableLocation,
            //     coordinates,
            // }))

            let payload: ICreatePostPayload = {
                poster_id: selectedPostedBy._id,
                poster_user_id: auth._id,
                posted_by_type: selectedPostedBy.type,
                caption: values.caption,
                // assets: JSON.stringify(assetsPayload),
                assets: fileHandles.map(m => ({
                    ...m,
                })),
                is_comments_enabled: !disableComments,
                tagged_in_post: [],
                is_location_enabled: !disableLocation,
                coordinates,
            }

            let response =
                await createPost(
                    // form_data,
                    payload,
                    {
                        access_token: auth.facebook_access_token,
                        id: auth.id,
                        social_provider: auth.social_provider
                    }
                )

            console.debug('after post request')

            if (response === null) {
                // TODO: Message about failed to create post
                console.debug('Failed to creating post')
                setIsSubmitting(false)
                return
            }

            let fileHandleResponse: IFile[] =
                fileHandles.filter(m => {
                    let r = response?.find(f => f.index === m.index)

                    if (r !== undefined) {
                        return {
                            ...r,
                            image_id: m.image_id,
                        }
                    }
                })

            // // TODO: send ids with index, and get ObjectId back and map to assets then send them back up one by one
            // let params: Record<string, string> = {
            //     poster_id: JSON.stringify(selectedPostedBy._id),
            //     poster_user_id: JSON.stringify(auth._id),
            //     posted_by_type: JSON.stringify(selectedPostedBy.type),
            //     caption: JSON.stringify(values.caption),
            //     // pin_type: pinTypeDisplayValue[1],
            //     // assets: JSON.stringify(assetsPayload),
            //     // assets: assetsPayload,
            //     is_comments_enabled: JSON.stringify(!disableComments),
            //     tagged_in_post: JSON.stringify([]),
            //     is_location_enabled: JSON.stringify(!disableLocation),
            //     coordinates: JSON.stringify(coordinates),
            // }

            // let res = await createPostOneFile(
            //     // assets[0].source,
            //     route.params[0].source,
            //     {
            //         access_token: auth.facebook_access_token,
            //         id: auth.id,
            //         social_provider: auth.social_provider,
            //     },
            //     params
            // )

            console.debug('before uploading assets')
            console.debug('route')
            console.debug(route)
            console.debug('response')
            console.debug(response)
            console.debug('filehandleres')
            console.debug(fileHandleResponse)

            // let res = await uploadAssetForPost(
            //     route.params[0].source,
            //     {
            //         id: auth.id,
            //         access_token: auth.facebook_access_token,
            //         social_provider: auth.social_provider,
            //         // TODO: fix route.params type
            //         // @ts-ignore
            //         asset_type: route.params[0].type,
            //         // // @ts-ignore
            //         // content_type: 'application/octet-stream',
            //         // // content_type: 'application/x-www-form-urlencoded',
            //         index: 1,
            //         // image_id: fileHandleResponse.map(m => m.image_id)[0],
            //         image_id: response[0].image_id,
            //     }
            // )

            let res = await uploadAssetFromPost(
                route.params[0].source,
                {
                    id: auth.id,
                    access_token: auth.facebook_access_token,
                    social_provider: auth.social_provider,
                    // TODO: fix route.params type
                    // @ts-ignore
                    asset_type: route.params[0].type,
                    // // @ts-ignore
                    // content_type: 'application/octet-stream',
                    // // content_type: 'application/x-www-form-urlencoded',
                    index: 1,
                    // image_id: fileHandleResponse.map(m => m.image_id)[0],
                    image_id: response[0].image_id,
                }
            )

            setIsSubmitting(false)
            // navigation.goBack()
            navigation.popToTop()
        } catch (error) {
            setIsSubmitting(false)
            console.debug(error)
        }
    }

    const ProviderSelect = () => {
        switch (selectedProvider) {
            default:
            case 0:
                return (
                    <Select
                        style={[
                            // styles.containerItem,
                            {
                                width: '100%',
                            }
                        ]}
                        label={i18n.t('post.create.posted_by_type')}
                        selectedIndex={postedByIndex}
                        value={selectedPostedBy.name}
                        onSelect={index => setPostedBySelect(index)}
                    >
                        {/* <SelectItem
                            title={i18n.t('post.create.posted_by_user')}
                        /> */}
                        {postedByChoices.map((m, i) => (
                            <SelectItem key={i} title={m.name} />
                        ))}
                    </Select>
                )
            case 1:
                return (
                    <Text>
                        Not implemented select zone
                    </Text>
                )
            case 2:
                return (
                    <Text>
                        Not implemented select municipality
                    </Text>
                )
        }
    }

    const LoadingIndicator = (props: any) => (
        <View style={[props.style, styles.indicator]}>
            {isSubmitting ?
                <Spinner size='small' /> :
                <Icon {...props} name='plus-square-outline' />
            }
        </View>
    )

    return (
        <ScrollView>
            <Layout style={styles.container}>
                <ProviderSelect />
            </Layout>
            <Formik
                initialValues={initStateFields}
                // validationSchema={validationSchema}
                // // onSubmit={values => onSubmit(values)}
                onSubmit={values => onSubmit(values)}
                // isInitialValid={false}
                initialErrors={{
                    caption: undefined
                }}
            // initialTouched={{
            //     name: false
            // }}
            // validateOnMount={true}
            >
                {({
                    values,
                    errors,
                    isValid,
                    touched,
                    handleChange,
                    handleBlur,
                    handleReset,
                    handleSubmit,
                }) => (
                        <Layout
                            style={styles.container}
                        >
                            <Input
                                label={i18n.t('post.create.caption')}
                                caption={i18n.t('common.required')}
                                captionIcon={InfoIcon}
                                status={
                                    isFieldValid(
                                        'caption',
                                        errors.caption,
                                        touched.caption
                                    ) ? 'basic' : 'danger'
                                }
                                multiline={true}
                                maxLength={100}
                                value={values.caption}
                                onChangeText={handleChange('caption')}
                            />
                            {/* <ImagePreviewCarousel
                                data={assets}
                                // data={values.assets}
                                // data={carousel_data}
                                divideItemWidthBy={3}
                                removeItem={removeAsset}
                            /> */}
                            {/* <Button
                                style={{
                                    marginTop: 10,
                                }}
                                appearance='outline'
                                // onPress={openPickerModal}
                                onPress={openCamera}
                            >
                                {i18n.t('post.model.asset_title')}
                            </Button> */}
                            <Layout
                                style={{
                                    marginTop: 20,
                                    flexDirection: 'column',
                                }}
                            >
                                <SwitchControl
                                    text={'Disable location'}
                                    value={disableLocation}
                                    handleValue={setDisableLocation}
                                />
                                <SwitchControl
                                    text={i18n.t('post.create.is_comments_enabled')}
                                    value={disableComments}
                                    handleValue={setDisableComments}
                                />
                            </Layout>
                            <Layout style={{
                                marginBottom: 20,
                                flexDirection: 'row',
                            }}>
                                <Button
                                    style={[
                                        styles.button,
                                        {
                                            marginRight: 20,
                                        }
                                    ]}
                                    appearance='outline'
                                    onPress={() => {
                                        setAssets([])
                                        handleReset()
                                    }}
                                >
                                    {i18n.t('common.reset')}
                                </Button>
                                <Button
                                    style={styles.button}
                                    disabled={!isValid}
                                    appearance='outline'
                                    accessoryLeft={LoadingIndicator}
                                    onPress={() => handleSubmit()}
                                >
                                    {i18n.t('common.post')}
                                </Button>
                            </Layout>
                        </Layout>
                    )}
            </Formik>
            {/* <ImageModal
                visible={isImageModalVisible}
                setVisible={setIsImageModalVisible}
                openPicker={openPicker}
            /> */}
        </ScrollView>
    )
}