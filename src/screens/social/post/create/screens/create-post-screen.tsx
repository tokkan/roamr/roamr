import { RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"
import { Layout, Spinner } from "@ui-kitten/components"
import React, { useState } from "react"
import { useSelector } from "react-redux"
import { LoaderScreen } from "./../../../../common/loader/loader-screen"
import { TopNavigationComponent } from "../../../../../components/top-navigation/top-navigation-stack"
import { i18n } from "../../../../../locales/localization"
import { IRootState } from "../../../../../store"
import { RootStackCreatePostRoutesParamList } from "../../../../routes"
import { PostScreenComponent } from "./post-screen-component"

type ScreenRouteProp = RouteProp<
    RootStackCreatePostRoutesParamList,
    'CreatePost'
>

type ScreenNavigationProp = StackNavigationProp<
    RootStackCreatePostRoutesParamList,
    'CreatePost'
>

interface IProps {
    route: ScreenRouteProp
    navigation: ScreenNavigationProp
}

export const CreatePostScreen: React.FC<IProps> = ({
    route,
    navigation,
}) => {
    const auth = useSelector((s: IRootState) => s.auth)
    // const [source, setSource] = useState(axios.CancelToken.source())
    const [isLoading, setIsLoading] = useState(false)
    const [isSubmitting, setIsSubmitting] = useState(false)

    return (
        <Layout
            style={{
                flex: 1,
            }}
        >
            <TopNavigationComponent
                navigation={navigation}
                title={i18n.t('post.create.title')}
            />
            <Layout style={{ flex: 1 }}>
                {isLoading ?
                    <LoaderScreen /> :
                    <PostScreenComponent
                        route={route}
                        navigation={navigation}
                    />
                }
            </Layout>
        </Layout>
    )
}