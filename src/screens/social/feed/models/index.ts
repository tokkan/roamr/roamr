export type AssetTypes =
    'image' |
    'image/jpg' |
    'image/jpeg' |
    'image/png' |
    'video'

export interface IAsset {
    source: string
    asset_type: AssetTypes
}

export interface IPostItem {
    _id: string
    username: string
    avatar: any
    caption: string
    images: string[]
    comments: IPostComment[]
    comment_count: number
    is_comments_enabled: boolean
    likes_count: number
    liked_by_user: boolean
    reshares: number
    coordinates: number[]
    disable_location: boolean
}

export interface IPostComment {
    _id: string
    username: string
    avatar: any
    comment: string
    likes: number
    replies: []
}

export interface IEventItem {
    _id: string
}

export interface IRenderFeedItem {
    // item: IFeedItem
    item: FeedItems
    index: number
}

export interface IRenderCommentItem {
    item: IPostComment
    index: number
}

export type FeedItemTypes =
    'post' |
    'event' |
    'system' |
    'eol_feed'

export type FeedItems =
    IFeedPostItem |
    IFeedEventItem

interface IFeedItem<T extends FeedItemTypes, I> {
    type: T
    item: I
}

interface IFeedPostItem extends IFeedItem<
    'post',
    IPostItem
    > { }

interface IFeedEventItem extends IFeedItem<
    'event',
    IEventItem
    > { }