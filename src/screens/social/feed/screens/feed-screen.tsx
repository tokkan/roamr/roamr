import React, { useEffect, useState } from 'react'
import { Layout } from '@ui-kitten/components'
import { Header, StackNavigationProp } from '@react-navigation/stack'
import { useSelector } from 'react-redux'
import { KeyboardAvoidingView, Platform } from 'react-native'
import { IRootState } from './../../../../store'
import { FeedItems } from '../models'
import { FeedItemList } from '../components/list/feed-item-list'
import { EmptyList } from './../../../common/empty/empty-list'
import { TopNavigationComponent } from './../../../../components/top-navigation/top-navigation'
import { getFeedForTimeline, getPostsRequest } from '../services/post'
import { AddIcon, MailboxUpIcon, MessageIcon, NotificationsIcon, SupportRequestIcon } from './../../../../models'
import { IData, OptionsListModal } from './../../../../components/modal/common/list/options-list-modal'
import { RootStackFeedRoutesParamList } from '../../../routes'
import { navigate } from '../../../../common/navigation'

type ScreenNavigationProp = StackNavigationProp<
    RootStackFeedRoutesParamList,
    'Feed'
>

interface IProps {
    navigation: ScreenNavigationProp
}

export const FeedScreen: React.FC<IProps> = ({
    navigation,
}) => {
    const auth = useSelector((s: IRootState) => s.auth)
    const [keyboardMargin, setKeyboardMargin] = useState(0)
    const [refreshing, setRefreshing] = useState(false)
    const [feedItems, setFeedItems] = useState<FeedItems[]>([])
    const [itemMenuVisible, setItemMenuVisible] = useState(false)
    const [data] = useState<IData[]>([
        {
            title: 'Report',
            icon: SupportRequestIcon,
            onPress: () => console.debug('Click on report action'),
            // onPress: () => navigation.navigate('Support'),
        }
    ])

    useEffect(() => {
        onGetPosts()
    }, [])

    const onGetPosts = async (
        limit: number = 20,
        skip: number = 0,
    ) => {
        let res = await getFeedForTimeline(
            limit,
            skip,
            {
                _id: auth._id,
                id: auth.id,
                access_token: auth.facebook_access_token,
                social_provider: auth.social_provider,
            },
        )

        if (res !== null) {
            // setPosts(res)
            setFeedItems(res)
        }
    }

    const onRefresh = async () => {
        setRefreshing(true)
        // setTimeout(() => {
        //     setRefreshing(false)
        // }, 1500);
        await onGetPosts()
        setRefreshing(false)
    }

    const renderList = () => {
        if (feedItems.length > 0) {
            return (
                <FeedItemList
                    navigation={navigation}
                    data={feedItems}
                    menuVisible={itemMenuVisible}
                    setMenuVisible={setItemMenuVisible}
                    setKeyboardMargin={setKeyboardMargin}
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                />
            )
        } else {
            return (
                <EmptyList
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                />
            )
        }
    }

    return (
        <Layout style={{ flex: 1 }}>
            <TopNavigationComponent
                navigation={navigation}
                title={'Feed'}
                hasMenuAction={true}
                hasDefaultTitle={true}
                rightAccessories={[
                    {
                        icon: AddIcon,
                        // action: () => navigation.navigate('CreatePost')
                        // action: () => navigation.navigate('Camera')
                        // action: () => navigate({
                        //     name: 'Home'
                        // })
                        action: () => navigate({
                            name: 'CreatePost',
                            params: [],
                        })
                        // action: () => console.debug('click')
                    },
                    {
                        icon: NotificationsIcon,
                        // action: () => navigation.navigate('Notifications')
                        action: () => console.debug('click')
                    },
                    {
                        icon: MailboxUpIcon,
                        // action: () => navigation.navigate('Messages')
                        action: () => console.debug('click')
                    },
                ]}
            />
            <KeyboardAvoidingView
                style={{ flex: 1 }}
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                // behavior='height'
                // @ts-ignore
                keyboardVerticalOffset={Header.HEIGHT + 64}
            >
                <Layout
                    style={{
                        // flex: 1,
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                    }}
                >
                    {renderList()}
                </Layout>
            </KeyboardAvoidingView>
            <OptionsListModal
                menuVisible={itemMenuVisible}
                setMenuVisible={setItemMenuVisible}
                data={data}
            />
        </Layout>
    )
}