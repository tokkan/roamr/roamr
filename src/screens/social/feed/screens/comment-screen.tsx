import React, { useState, useRef } from 'react'
import { Layout, Divider, Button, Text, ListItem, Avatar, Input } from '@ui-kitten/components'
import { FlatList, View, KeyboardAvoidingView, Platform } from 'react-native'
import { useSelector } from 'react-redux'
import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import { FilledLikeIcon, LikeIcon } from './../../../../models'
import { styles } from '../styles/styles'
import { IRootState } from './../../../../store'
import { TopNavigationComponent } from './../../../../components/top-navigation/top-navigation-stack'
import { RootStackFeedRoutesParamList } from '../../../routes'
import { IPostComment } from '../models'

export interface IRenderCommentItem {
    item: IPostComment
    index: number
}

type ScreenRouteProp = RouteProp<
    RootStackFeedRoutesParamList,
    'PostCommentList'
>

type ScreenNavigationProp = StackNavigationProp<
    RootStackFeedRoutesParamList,
    'PostCommentList'
>

interface IProps {
    route: ScreenRouteProp
    navigation: ScreenNavigationProp
}

export const CommentScreen: React.FC<IProps> = ({
    route,
    navigation,
}) => {
    // const { post, comments } = route.params
    const { post } = route.params
    const auth = useSelector((s: IRootState) => s.auth)
    const [keyboardMargin, setKeyboardMargin] = useState(3)
    const [comments, setComments] = useState<IPostComment[]>([])

    const getAvatar = () => {
        if (auth.avatar === undefined ||
            auth.avatar === null ||
            auth.avatar.length <= 0) {
            // return require('./../../../icons/hiking.png')
            return require('./../temp.png')
        } else {
            return {
                uri: auth.avatar
            }
        }
    }

    const LikeAction = (props: any) => {
        const [isLiked, setIsLiked] = useState(false)
        const pulseIconRef = useRef()

        return (
            <Button
                appearance='ghost'
                size='small'
                status='basic'
                accessoryLeft={isLiked ? FilledLikeIcon : LikeIcon}
                onPress={() => setIsLiked(!isLiked)}
            />
        )
    }

    const CommentAvatar = (item: IPostComment) => (
        // <Avatar
        //     style={styles.cardAvatar}
        //     size='tiny'
        //     source={{
        //         uri: 'http://placekitten.com/200/300'
        //     }}
        // />
        <Text category='p2' appearance='hint'>
            {item.username}
        </Text>
    )

    const renderCommentItems = ({ item, index }: IRenderCommentItem) => {
        return (
            <ListItem
                key={index}
                style={styles.cardCommentListItem}
                // title={item.comment}
                description={item.comment}
                accessoryLeft={() => CommentAvatar(item)}
                accessoryRight={LikeAction}
            />
        )
    }

    return (
        <KeyboardAvoidingView
            style={{ flex: 1 }}
            behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        >
            <Layout
                style={{
                    flex: 1,
                }}
            >
                <TopNavigationComponent
                    navigation={navigation}
                    title={'Comments'}
                />
                <Layout
                    style={[
                        styles.cardContentContainer,
                        {
                            paddingVertical: 5,
                            paddingBottom: 10,
                        }
                    ]}
                    level='2'
                >
                    <View style={{
                        flexDirection: 'row',
                        // width: '100%',
                    }}>
                        <Avatar
                            style={styles.cardAvatar}
                            size='small'
                            source={{
                                uri: post.avatar
                            }}
                        />
                        <Text
                            style={{
                                // flexDirection: 'row',
                                // justifyContent: 'space-between',
                                // textAlignVertical: 'center',
                                maxWidth: '90%',
                                marginLeft: 5,
                            }}
                            appearance='hint'
                        >
                            <Text
                                category='p2'
                                style={{
                                    marginRight: 5, paddingRight: 5
                                }}
                                onPress={() => console.log('click on username')}
                            >
                                {`${post.username} `}
                            </Text>
                            <Text
                                appearance='hint'
                            >
                                {post.caption}
                            </Text>
                        </Text>
                    </View>
                </Layout>
                <FlatList
                    style={{ width: '100%' }}
                    data={comments}
                    renderItem={renderCommentItems}
                    ItemSeparatorComponent={Divider}
                    keyExtractor={item => item._id}
                />
                <Layout style={{
                    flexDirection: 'row',
                    // justifyContent: 'center',
                    // alignContent: 'center',
                    alignItems: 'center',
                    marginTop: 10,
                    marginBottom: keyboardMargin,
                    paddingHorizontal: 10,
                }}>
                    <Avatar
                        style={styles.cardAvatar}
                        size='tiny'
                        source={getAvatar()}
                    />
                    <Input
                        style={{ flex: 1 }}
                        placeholder='Write comment...'
                        onFocus={() => setKeyboardMargin(36)}
                        onBlur={() => setKeyboardMargin(3)}
                    // value={values.search}
                    // onChangeText={onHandleChange('search')}
                    />
                </Layout>
            </Layout>
        </KeyboardAvoidingView>
    )
}