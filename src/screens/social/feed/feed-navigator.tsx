import React from 'react'
import { createStackNavigator } from "@react-navigation/stack"
import { FeedScreen } from './screens/feed-screen'
import { CommentScreen } from './screens/comment-screen'
import { RootStackFeedRoutesParamList } from '../../routes'
// import { CreatePostScreen } from '../post/create/screens/create-post-screen'
// import { CameraScreen } from './../../../components/camera/camera-screen'
// import { CreatePostNavigator } from '../post/create/create-post-navigator'

const Stack = createStackNavigator<RootStackFeedRoutesParamList>()

export const FeedNavigator = () => {
    return (
        <Stack.Navigator
            initialRouteName={'Feed'}
            headerMode='none'
        >
            <Stack.Screen
                name={'Feed'}
                component={FeedScreen}
            />
            <Stack.Screen
                name={'PostCommentList'}
                component={CommentScreen}
            />
            {/* <Stack.Screen
                name={'CreatePost'}
                component={CreatePostNavigator}
            />
            <Stack.Screen
                name={'Camera'}
                component={CameraScreen}
            /> */}
        </Stack.Navigator>
    )
}