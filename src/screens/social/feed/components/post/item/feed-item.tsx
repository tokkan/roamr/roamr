import React, { Dispatch, FC, SetStateAction } from 'react'
import { StackNavigationProp } from '@react-navigation/stack'
import { Avatar, Divider, Layout, Text, TopNavigation, TopNavigationAction } from '@ui-kitten/components'
import { styles } from '../../../styles/styles'
import { RootStackFeedRoutesParamList } from '../../../../../routes'
import { IAsset, IPostItem } from '../../../models'
import { Image } from 'react-native'
import { FeedItemFooter } from '../footer/feed-item-footer'
import { VerticalDotsIcon } from '../../../../../../models'
import axios from 'axios'
import { FeedItemCarousel } from './feed-item-carousel'

type ScreenNavigationProp = StackNavigationProp<
    RootStackFeedRoutesParamList,
    'Feed'
>

interface IProps {
    navigation: ScreenNavigationProp
    menuVisible: boolean
    setMenuVisible: Dispatch<SetStateAction<boolean>>
    setKeyboardMargin: Dispatch<SetStateAction<number>>
    item: IPostItem
}

export const FeedItem: FC<IProps> = ({
    navigation,
    menuVisible,
    setMenuVisible,
    setKeyboardMargin,
    item,
}) => {
    const [images, setImages] = React.useState<IAsset[]>([])
    React.useEffect(() => {
        getImage()
        // console.debug('useFX images')
        // console.debug(images.length)
    }, [item])

    React.useEffect(() => {
        console.debug('useFX images 2')
        console.debug(images.length)
        // images.forEach((f, i) => {
        //     // if (f.source.length < 1000) {
        //     console.debug(`images foreach: ${i}`)
        //     item.images.forEach(f => console.debug(f))
        //     if (f.source.length > 0)
        //         console.debug(f.source.split('').slice(0, 20))
        //     // }
        // })
    }, [images])

    const getImage = async () => {
        // let res = await axios.get(`/storage?id=${item.images.join(',')}`)
        // console.debug('getImage')
        // console.debug(res.data)

        // item.images.forEach(async f => {
        //     console.debug('getImage')
        //     console.debug(f)
        //     let res = await axios.get(`/storage?id=${f}`)
        //     setImages([...images, res.data])
        // })

        let req =
            item.images.map(async m => await axios.get(`/storage?id=${m}`))

        console.log(getImage.name)
        // console.log(req)

        let res = await axios.all(req)
        // let res = await axios.all([
        //     axios.get(`/storage?id=${item.images[0]}`),
        // ])

        if (res) {
            // console.debug(res[0].headers)
            // let temp = res.map(m => {
            //     return {
            //         source: m.data,
            //         metadata: m.headers
            //     }
            // })

            // TODO: Check signature of m here. is data invalid or not?

            // let assetArray: IAsset[] = res.map(m => ({
            //     source: m.data,
            //     asset_type: m.headers['asset_type'],
            // }))
            let assetArray: IAsset[] = res.map(m => {
                return {
                    source: m.data,
                    // source: Buffer.from(m.data).toString('base64'),
                    // source: (m.data as Buffer).toString('base64'),
                    // source: (m.data as Buffer).buffer,
                    asset_type: m.headers['asset_type'],
                }
            })
            setImages(assetArray)
            // setImages([...res.map(m => m.data)])
        }
    }

    const toggleMenu = () => {
        setMenuVisible(!menuVisible)
    }

    const OptionsAction = () => {
        return (
            <TopNavigationAction
                icon={VerticalDotsIcon}
                onPress={toggleMenu}
            />
        )
    }

    return (
        <>
            <TopNavigation
                title={props => (
                    <Text category='s1' style={{ alignSelf: 'center' }}>
                        {item.username}
                    </Text>)}
                accessoryLeft={props =>
                    item.avatar.length > 0 ?
                        <Avatar
                            style={styles.cardAvatar}
                            size='tiny'
                            source={{
                                uri: item.avatar
                            }}
                        /> : <></>
                    // <Avatar
                    //     style={styles.cardAvatar}
                    //     size='tiny'
                    //     source={{
                    //         uri: item.avatar
                    //     }}
                    // />
                }
                accessoryRight={OptionsAction}
            />
            <Layout
                // style={styles.imageContainer}
                style={{
                    flex: 1,
                    width: '100%',
                    // height: '100%',
                }}
                level='2'
            >
                {images.length > 0 &&
                    // <Image
                    //     style={styles.cardImage}
                    //     defaultSource={require('./default-avatar-4.png')}
                    //     source={{
                    //         uri: `data:image/jpeg;base64,${images[0]}`
                    //     }}
                    //     resizeMode='contain'
                    // />
                    <FeedItemCarousel
                        data={images}
                    />
                }
            </Layout>
            <FeedItemFooter
                navigation={navigation}
                item={item}
                setKeyboardMargin={setKeyboardMargin}
            />
            <Divider />
        </>
    )
}