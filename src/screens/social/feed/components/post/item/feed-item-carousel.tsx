import React, { FC, useRef, useState } from 'react'
import { Dimensions, Image } from 'react-native'
import Carousel from 'react-native-snap-carousel'
import { Video } from 'expo-av'
import { IAsset } from '../../../models'
import { styles } from '../../../styles/styles'

export type ItemTypes =
    'image' |
    'video'

// interface IItem {
//     source: string
//     type: AssetTypes
// }

interface IProps {
    // data: IItem[]
    // data: string[]
    data: IAsset[]
}

export const FeedItemCarousel: FC<IProps> = ({
    data,
}) => {
    const ref = useRef()
    const [activeSlide, setActiveSlide] = useState(0)
    const [screenWidth, setScreenWidth] =
        useState(Dimensions.get('window').width)
    const [sliderWidth, setSliderWidth] =
        useState(screenWidth)
    const [itemWidth, setItemWidth] =
        useState((screenWidth) / 1)

    const renderItem: FC<{
        // item: IItem,
        item: IAsset,
        index: number
    }> = ({
        item,
        index,
    }) => {
            if (item.asset_type.startsWith('image')) {
                return (
                    <Image
                        key={index}
                        style={styles.cardImage}
                        defaultSource={require('./default-avatar-4.png')}
                        source={{
                            // uri: `data:image/jpeg;base64,${item.src}`
                            // uri: `data:image/jpeg;base64,${item}`
                            uri: `data:${item.asset_type};base64,${item.source}`
                        }}
                        resizeMode='contain'
                    />
                )
            } else if (item.asset_type.startsWith('video')) {
                return (
                    <Video
                        key={index}
                        // style={styles.cardImage}
                        style={{
                            minWidth: 300,
                            width: '100%',
                            height: 300,
                        }}
                        posterSource={require('./default-avatar-4.png')}
                        source={{
                            uri: `data:${item.asset_type};base64,${item.source}`
                        }}
                        rate={1.0}
                        volume={1.0}
                        isMuted={false}
                        // resizeMode={'cover'}
                        resizeMode={'contain'}
                        shouldPlay
                        isLooping
                    />
                )
            }
            else {
                return (<></>)
            }
        }

    return (
        <Carousel
            data={data}
            renderItem={renderItem}
            layout='default'
            // ref={ref.current}
            sliderWidth={sliderWidth}
            // sliderHeight={300}
            itemWidth={itemWidth}
            hasParallaxImages={true}
            onSnapToItem={index => setActiveSlide(index)}
            firstItem={1}
            horizontal={true}
            // loop={true}
            enableSnap={true}
        />
    )
}