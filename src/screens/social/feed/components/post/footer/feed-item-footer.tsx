import React, { useState, useRef, useEffect } from 'react'
import { View, FlatList, Pressable, TouchableOpacity } from 'react-native'
import {
    Button, Layout, Text, Avatar, Input, ListItem, Divider
} from '@ui-kitten/components'
import { useSelector } from 'react-redux'
import { StackNavigationProp } from '@react-navigation/stack'
import { i18n } from './../../../../../../locales/localization'
import {
    FilledLikeIcon, LikeIcon, CommentIcon, MessageIcon, MapIcon
} from './../../../../../../models'
import { IRootState } from './../../../../../../store'
import { styles } from '../../../styles/styles'
import { IPostComment, IPostItem, IRenderCommentItem } from '../../../models'
import { RootStackFeedRoutesParamList } from './../../../../../routes'

type ScreenNavigationProp = StackNavigationProp<
    RootStackFeedRoutesParamList,
    'Feed'
>

interface IProps {
    navigation: ScreenNavigationProp,
    item: IPostItem,
    setKeyboardMargin: React.Dispatch<React.SetStateAction<number>>
    // props: any
}

export const FeedItemFooter: React.FC<IProps> = ({
    navigation,
    item,
    setKeyboardMargin,
    // props,
}) => {

    const auth = useSelector((s: IRootState) => s.auth)
    const [isLiked, setIsLiked] = useState(false)
    const [showPostText, setShowPostText] = useState(true)

    // useEffect(() => {
    //     if (item.username.length + item.content.length > 90) {
    //         setShowPostText(false)
    //     }
    // }, [])

    const getAvatar = () => {
        if (auth.avatar === undefined ||
            auth.avatar === null ||
            auth.avatar.length <= 0) {
            return require('./../../../temp.png')
        } else {
            return {
                uri: auth.avatar
            }
        }
    }

    const ShowPostTextAction = () => {
        if (!showPostText) {
            return (
                <TouchableOpacity
                    onPress={() => setShowPostText(true)}
                >
                    <Text
                        category='p2'
                    // appearance='hint'
                    >
                        Show more...
                    </Text>
                </TouchableOpacity>
            )
        } else {
            return (<></>)
        }
    }

    const ShowCommentsAction = () => {
        if (item.comment_count > 0) {
            return (
                <TouchableOpacity
                    onPress={() => navigation.navigate('PostCommentList', {
                        post: item,
                        // comments: item.comments,
                    })}
                >
                    <Text
                        category='p2'
                    // appearance='hint'
                    >
                        {/* {`View all ${item.comments.length} comments`} */}
                        {i18n.t('timeline.view_comments', {
                            // count: item.comments.length,
                            count: item.comment_count,
                        })}
                    </Text>
                </TouchableOpacity>
            )
        } else {
            return <></>
        }
    }

    const LikeAction = (props: any) => {

        const [isLiked, setIsLiked] = useState(false)
        const pulseIconRef = useRef()

        return (
            <Button
                style={{
                    borderRadius: 200,
                }}
                appearance='ghost'
                size='small'
                status='basic'
                accessoryLeft={isLiked ? FilledLikeIcon : LikeIcon}
                onPress={() => setIsLiked(!isLiked)}
            />
        )
    }

    const CommentAvatar = (item: IPostComment) => (
        // <Avatar
        //     style={styles.cardAvatar}
        //     size='tiny'
        //     source={{
        //         uri: 'http://placekitten.com/200/300'
        //     }}
        // />
        <Text category='p2' appearance='hint'>
            {item.username}
        </Text>
    )

    const renderCommentItems = ({ item, index }: IRenderCommentItem) => {
        return (
            <ListItem
                key={index}
                style={styles.cardCommentListItem}
                // title={item.comment}
                description={item.comment}
                accessoryLeft={() => CommentAvatar(item)}
                accessoryRight={LikeAction}
            />
        )
    }

    return (
        <View>
            {item.likes_count > 0 ?
                <Layout style={styles.cardLikesContainer}>
                    <Text category='p2' appearance='hint'>
                        {`${item.likes_count} has liked`}
                    </Text>
                </Layout>
                : <View></View>}
            <View
                // {...props} 
                style={[
                    // props.style, 
                    styles.footerContainer
                ]}
            >
                <Button
                    style={styles.footerControl}
                    appearance='ghost'
                    size='medium'
                    status='basic'
                    accessoryLeft={isLiked ? FilledLikeIcon : LikeIcon}
                    onPress={() => setIsLiked(!isLiked)}
                />
                <Button
                    style={styles.footerControl}
                    appearance='ghost'
                    size='medium'
                    status='basic'
                    accessoryLeft={CommentIcon}
                    onPress={() => console.log('Comment icon click')}
                />
                <Button
                    style={styles.footerControl}
                    appearance='ghost'
                    size='medium'
                    status='basic'
                    accessoryLeft={MessageIcon}
                />
                {!item.disable_location &&
                    <Button
                        style={styles.footerControl}
                        appearance='ghost'
                        size='medium'
                        status='basic'
                        accessoryLeft={MapIcon}
                    />
                }
            </View>
            <Layout
                style={styles.cardContentContainer}
            >
                <Text
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        textAlignVertical: 'center',
                    }}
                    appearance='hint'
                    numberOfLines={showPostText ? undefined : 2}
                >
                    <Text
                        category='p2'
                        style={{
                            marginRight: 5, paddingRight: 5
                        }}
                        onPress={() => console.log('click on username')}
                    >
                        {`${item.username} `}
                    </Text>
                    <Text
                        appearance='hint'
                    >
                        {item.caption}
                    </Text>
                </Text>
                <ShowPostTextAction />
            </Layout>
            
            <Layout style={styles.cardCommentContainer}>
                {item.comment_count > 0 &&
                    <ShowCommentsAction />
                }
                <Layout style={{
                    flexDirection: 'row',
                    // justifyContent: 'center',
                    // alignContent: 'center',
                    alignItems: 'center',
                    marginTop: 10,
                }}>
                    <Avatar
                        style={styles.cardAvatar}
                        size='tiny'
                        source={getAvatar()}
                    />
                    <Input
                        style={{ flex: 1 }}
                        placeholder='Write comment...'
                        onFocus={() => setKeyboardMargin(36)}
                        onBlur={() => setKeyboardMargin(0)}
                        size='small'
                    // value={values.search}
                    // onChangeText={onHandleChange('search')}
                    />
                </Layout>
            </Layout>
        </View>
    )
}