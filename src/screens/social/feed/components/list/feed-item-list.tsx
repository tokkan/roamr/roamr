import React, { Dispatch, FC, SetStateAction } from 'react'
import { RefreshControl, FlatList, Image } from 'react-native'
import { StackNavigationProp } from '@react-navigation/stack'
import { Avatar, Divider, Layout, Text, TopNavigation, TopNavigationAction } from '@ui-kitten/components'
import { i18n } from '../../../../../locales/localization'
import { FeedItems, IRenderFeedItem } from '../../models'
import { FeedItem } from '../post/item/feed-item'
import { RootStackFeedRoutesParamList } from './../../../../routes'

type ScreenNavigationProp = StackNavigationProp<
    RootStackFeedRoutesParamList,
    'Feed'
>

interface IProps {
    navigation: ScreenNavigationProp
    data: FeedItems[]
    menuVisible: boolean
    setMenuVisible: Dispatch<SetStateAction<boolean>>
    setKeyboardMargin: Dispatch<SetStateAction<number>>
    refreshing: boolean
    onRefresh: any
}

export const FeedItemList: FC<IProps> = ({
    navigation,
    data,
    menuVisible,
    setMenuVisible,
    onRefresh,
    refreshing,
    setKeyboardMargin,
}) => {
    const renderFeedItems = ({
        item
    }: IRenderFeedItem) => {
        switch (item.type) {
            case 'post':
                return (
                    <FeedItem
                        navigation={navigation}
                        item={item.item}
                        menuVisible={menuVisible}
                        setMenuVisible={setMenuVisible}
                        setKeyboardMargin={setKeyboardMargin}
                    />
                )

            default:
                return (
                    <></>
                )
        }
    }

    return (
        <FlatList
            style={{
                width: '100%',
                height: '100%',
            }}
            data={data}
            renderItem={renderFeedItems}
            keyExtractor={item => item.item._id}
            // onScroll={event =>}
            // scrollEventThrottle={0}
            refreshing={refreshing}
            refreshControl={
                <RefreshControl
                    // colors={[]}
                    // progressBackgroundColor={theme['background-basic-color-1']}
                    // tintColor={theme['text-primary-color']}
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                />
            }
        />
    )
}