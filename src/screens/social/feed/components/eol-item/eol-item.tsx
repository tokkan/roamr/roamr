import React from 'react'
import { View } from 'react-native'
import { Divider, Text } from '@ui-kitten/components'

export const EOLItem = () => {
    return (
        <>
            <View
            style={{
                height: 300,
            }}
            >
                <Text>
                    Last item of user items, add lottie animation or similar
                </Text>
            </View>
            <Divider />
        </>
    )
}