import axios, { CancelToken } from 'axios'
import { ProviderTypes } from '../../../../store/auth'
import { FeedItems } from '../models'

interface IPostResponse {
    _id: string,
    poster_id: string,
    poster_user_id: string,
    posted_by_type: "user",
    caption: string,
    // images: [
    //     {
    //         modificationDate: "1598624571000",
    //         width: number,
    //         size: number,
    //         mime: "image/jpeg",
    //         data: string,
    //         height: number,
    //         // path: string
    //     }
    // ],
    images: string[],
    is_comments_enabled: boolean,
    tagged_in_post: string[]
}


export type FeedItemTypes =
    'post' |
    'feed'

export interface IFeedItem<T extends FeedItemTypes, I> {
    type: T
    item: I
}

export interface IPostFeedItem extends IFeedItem<
    'post',
    IPostResponse
    > { }

export type FeedResponses =
    IPostFeedItem

export const getFeedForTimeline = async (
    limit: number,
    skip: number,
    headers: {
        _id: string
        id: string
        access_token: string
        social_provider: string
    },
    cancelToken?: CancelToken,
) => {
    const {
        _id,
        access_token,
        id,
        social_provider
    } = headers
    try {
        let response =
            await axios.get<FeedItems[]>(`posts/timeline?limit=${limit}skip=${skip}`,
                {
                    headers: {
                        _id: _id,
                        id: id,
                        access_token: access_token,
                        social_provider: social_provider,
                    },
                    cancelToken,
                })

        if (response.status === 200) {
            return response.data
        } else {
            return null
        }
    } catch (error) {
        console.debug(error)
        return null
    }
}

/**
 * 
 * @param _id mid
 * @param user_id fid 
 * @param access_token f access_token 
 * @param social_provider provider
 * @param limit limit
 * @param skip skip
 */
export const getPostsRequest = async (
    _id: string,
    user_id: string,
    access_token: string,
    social_provider: ProviderTypes,
    limit: number = 20,
    skip: number = 0,
) => {
    try {
        let response =
            await axios.get<IPostResponse[]>(`posts?limit=${limit}skip=${skip}`,
                {
                    headers: {
                        _id: _id,
                        user_id: user_id,
                        access_token: access_token,
                        social_provider: social_provider,
                    },
                    // cancelToken: source.token
                })

        if (response.status === 200) {
            return response.data
        } else {
            return null
        }
    } catch (error) {
        console.debug(error)
        return null
    }
}