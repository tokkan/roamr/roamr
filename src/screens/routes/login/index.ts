import { FirebaseAuthTypes } from "@react-native-firebase/auth"

export type RootStackLoginRoutesParamList = {
    ['Login']: undefined
    ['Register']: {
        credential: FirebaseAuthTypes.UserCredential,
        facebookCredential: FirebaseAuthTypes.AuthCredential,
        hasRealName: boolean,
        first_name: string,
        last_name: string,
    }
}
