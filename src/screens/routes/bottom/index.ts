import { IPostComment, IPostItem } from "../../social/feed/models"
import { INavigationAction } from "../base"

export type RootStackBottomRoutesParamList = {
    ['Home']: undefined
    ['PostCommentList']: {
        post: IPostItem
        // comments: IPostComment[]
    }
    // ['CreatePost']: undefined
    // // ['Camera']: undefined
    // // ['CameraPreview']: {
    // //     type: 'image' | 'video',
    // //     asset: ICameraRouteParam,
    // // }
    ['Search']: undefined
    ['BarcodeScanner']: undefined
    ['BarcodeResult']: {
        // barcode_event: BarCodeReadEvent
    }
    ['SearchUserResult']: {
        user_id: string
        username: string
        full_name: string
        avatar: string
        avatar_url: string
    }
    ['Business']: undefined
    ['Pin']: undefined
    ['Zone']: undefined
    ['Municipality']: undefined
    ['Map']: undefined
    ['Events']: undefined
    ['UserSelf']: undefined
}

export interface IHomeNavigationAction extends INavigationAction<
    'Home',
    undefined
    > { }