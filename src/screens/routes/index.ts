import { IHomeNavigationAction } from './bottom'
import { ICreatePostNavigationAction } from './post'

export * from './bottom'
export * from './chat'
export * from './feed'
export * from './login'
export * from './post'
export * from './routes'

export type NavigationActions =
    IHomeNavigationAction |
    ICreatePostNavigationAction