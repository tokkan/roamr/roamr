import { INavigationAction } from "../base";

export interface ICameraRouteParam {
    source: string
    name: string
    type: string // ContentTypes
}

export type RootStackCameraRoutesParamList = {
    ['Camera']: undefined
    ['CameraPreview']: {
        type: 'image' | 'video',
        asset: ICameraRouteParam,
    }
}

export interface ICameraNavigationAction extends INavigationAction<
    'Camera',
    ICameraRouteParam[]
    > { }