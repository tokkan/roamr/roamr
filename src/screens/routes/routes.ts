export type ScreenRouteTypes =
    HomeScreenRouteTypes |
    SearchScreenRouteTypes |
    MapScreenRouteTypes |
    EventScreenRouteTypes |
    UserScreenRouteTypes |
    NotificationScreenRouteTypes |
    MessagesScreenRouteTypes |
    ProfileScreenRouteTypes |
    FeedScreenRouteTypes |
    SettingsRouteTypes

export type HomeScreenRouteTypes =
    'Home'

export type FeedScreenRouteTypes =
    'Feed' |
    'PostCommentList' |
    'CreatePost'

export type SearchScreenRouteTypes =
    'Search'

export type MapScreenRouteTypes =
    'Map'

export type EventScreenRouteTypes =
    'Event'

export type UserScreenRouteTypes =
    'User'

export type NotificationScreenRouteTypes =
    'Notifications'

export type MessagesScreenRouteTypes =
    'Messages' |
    'Chat' |
    'NewInstantMessage' |
    'NewGroupChat'

export type ProfileScreenRouteTypes =
    'UserNavigator' |
    'Business' |
    'Pin' |
    'Zone' |
    'Municipality'

export type SettingsRouteTypes =
    'Settings'