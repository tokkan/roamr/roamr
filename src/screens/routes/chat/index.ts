interface IChatParams<A extends 'open_chat' | 'new_chat', T> {
    action: A
    payload: T
}

interface IOpenChatParams extends IChatParams<
    'open_chat',
    {
        chat_id: string
    }
    > { }

interface INewChatParams extends IChatParams<
    'new_chat',
    {
        _id: string
        username: string
        avatar: string
    }
    > { }

export type RootStackMessagesRoutesParamList = {
    ['Messages']: undefined
    // ['Chat']: {
    //     action: 'open_chat' | 'new_chat'
    //     _id: string
    //     user_id: string
    //     username: string
    //     avatar: string
    // }
    ['Chat']: IOpenChatParams | INewChatParams
    ['ChatRoom']: undefined
    ['NewInstantMessage']: undefined
    ['NewGroupChat']: undefined
}