import { INavigationAction } from "../base";

export interface ICameraRouteParam {
    source: string
    name: string
    type: string // ContentTypes
}

export type RootStackCreatePostRoutesParamList = {
    ['Camera']: undefined
    ['CameraPreview']: {
        type: 'image' | 'video',
        asset: ICameraRouteParam,
    }
    ['CreatePost']: ICameraRouteParam[]
}

export interface ICreatePostNavigationAction extends INavigationAction<
    'CreatePost',
    ICameraRouteParam[]
    > { }