export interface INavigationAction<N, P> {
    name: N,
    params?: P
}

// export interface INavigationAction<T> {
//     <K extends keyof T>(action: { type: K, payload: T[K] }): () => T
// }
