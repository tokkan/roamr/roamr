import { IPostComment, IPostItem } from "../../social/feed/models"

export type RootStackFeedRoutesParamList = {
    ['Feed']: undefined
    ['PostCommentList']: {
        post: IPostItem
        // comments: IPostComment[]
    }
}

